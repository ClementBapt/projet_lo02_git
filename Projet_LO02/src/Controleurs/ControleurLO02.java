package Controleurs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;

import projet_LO02.*;
import Vues.*;

public class ControleurLO02 {
	
	public ControleurLO02(JButton btnSoumettre, JButton btnArreter, JButton btnTrickDeck, JButton btnTrickPile, JButton btnCdJ1, JButton btnCgJ1, 
			JButton btnCdJ2, JButton btnCgJ2, JButton btnCdJ3, JButton btnCgJ3, JButton btnThProp, JButton btnConsole, Partie p, VueGraphique vg) {
		
		btnArreter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				p.arreterPartie();

			}
		});
		
		btnSoumettre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				vg.setASoumis(true);
				
				
			}
		});
		
		btnTrickDeck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				p.setChoix(2);
				vg.setAChoisi(true);
				
			}
		});
		
		btnTrickPile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				p.setChoix(1);
				vg.setAChoisi(true);
				
			}
		});
		
		btnThProp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				p.setChoixEchange(3);
				vg.setAChoisi(true);
				
			}
		});
		
		btnCdJ1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if(p.getJoueurActuel() == 0) {
					p.setChoixEchange(2);
					vg.setAChoisi(true);
				}
				else {
					p.setChoix(2);
					vg.setAChoisi(true);
				}
				
				
			}
		});
		
		btnCgJ1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if(p.getJoueurActuel() == 0) {
					p.setChoixEchange(1);
					vg.setAChoisi(true);
				}
				else {
					p.setChoix(1);
					vg.setAChoisi(true);
				}
			}
		});
		
		btnCdJ2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if(p.getJoueurActuel() == 1) {
					p.setChoixEchange(2);
					vg.setAChoisi(true);
				}
				else if(p.getJoueurActuel() == 0){
					p.setChoix(2);
					vg.setAChoisi(true);
				}
				else {
					p.setChoix(4);
					vg.setAChoisi(true);
				}
			}
		});
		
		btnCgJ2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if(p.getJoueurActuel() == 1) {
					p.setChoixEchange(1);
					vg.setAChoisi(true);
				}
				else if(p.getJoueurActuel() == 0){
					p.setChoix(1);
					vg.setAChoisi(true);
				}
				else {
					p.setChoix(3);
					vg.setAChoisi(true);
				}
			}
		});
		
		btnCdJ3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if(p.getJoueurActuel() == 2) {
					p.setChoixEchange(2);
					vg.setAChoisi(true);
				}
				else {
					p.setChoix(4);
					vg.setAChoisi(true);
				}
			}
		});
		
		btnCgJ3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if(p.getJoueurActuel() == 2) {
					p.setChoixEchange(1);
					vg.setAChoisi(true);
				}
				else {
					p.setChoix(3);
					vg.setAChoisi(true);
				}
			}
		});
		
		btnConsole.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				p.changerDeVue(true);

			}
		});
		
	}

}
