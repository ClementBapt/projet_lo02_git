package Vues;
import projet_LO02.*;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Robot;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;

import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import Controleurs.*;
import javax.swing.JTextArea;
import javax.swing.JInternalFrame;

import java.awt.AWTException;
import java.awt.Choice;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class VueGraphique implements Observer{

	private JFrame frmTOHT;
	private Partie p;
	private JTextArea ta;
	private JOptionPane dBox;
	private JButton btnFinPartie;
	private JTextField entreeText;
	private JButton btnSoumettre;
	private Boolean aSoumis;
	private Boolean aChoisi;
	private int choix;
	private int choixEchange;
	
	private JLabel CgJ1;
	private JLabel CdJ1;
	private JLabel CgJ2;
	private JLabel CdJ2;
	private JLabel CgJ3;
	private JLabel CdJ3;
	private JLabel thProp;
	private JLabel lblTrickdeck;
	private JLabel lblTrickpile;
	private JLabel labelPoints_1;
	private JLabel labelPoints_2;
	private JLabel labelPoints_3;
	private JLabel lblJoueur_1;
	private JLabel lblJoueur_2;
	private JLabel lblJoueur_3;
	
	private JButton btnTrickDeck;
	private JButton btnTrickPile;
	private JButton btnCdJ1;
	private JButton btnCgJ1;
	private JButton btnCdJ2;
	private JButton btnCgJ2;
	private JButton btnCdJ3;
	private JButton btnCgJ3;
	private JButton btnThProp;
	private JButton btnConsole;
	
	private final String urlCardBack = "Images/DosDeCarte.png";
	private final String urlCardBackRotated = "Images/DosDeCarteRotation.png";
	
	private ImageIcon backCard;
	private ImageIcon backCardRotated;
	
	
	private Joueur interJoueur;
	private Joueur[] interJTab;
	private StrategyReelle stratR;
	private int interInt;
	private String interString;
	private Prop interCarteGauche;
	private Prop interCarteDroite;

	/**
	 * Create the application.
	 */
	public VueGraphique() {
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTOHT = new JFrame();
		frmTOHT.getContentPane().setBackground(new Color(0, 100, 0));
		frmTOHT.setBackground(new Color(0, 100, 0));
		frmTOHT.setTitle("The Other Hat Trick Virtual Game");
		frmTOHT.setBounds(100, 100, 1010, 600);
		frmTOHT.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTOHT.getContentPane().setLayout(null);
		
		this.dBox = new JOptionPane(); 
		
		this.ta = new JTextArea();
		ta.setBackground(new Color(240, 255, 255));
		ta.setBounds(60, 359, 855, 115);
		frmTOHT.getContentPane().add(ta);
		
		JLabel lblNewLabel = new JLabel(new ImageIcon("Images/logo.png"));
		lblNewLabel.setBounds(380, 6, 200, 90);
		frmTOHT.getContentPane().add(lblNewLabel);
		
		this.btnFinPartie = new JButton("Arrêter la partie");
		this.btnFinPartie.setBackground(Color.RED);
		this.btnFinPartie.setBounds(813, 16, 125, 63);
		frmTOHT.getContentPane().add(this.btnFinPartie);
		
		
		entreeText = new JTextField();
		entreeText.setBounds(60, 487, 726, 26);
		frmTOHT.getContentPane().add(entreeText);
		entreeText.setColumns(10);
		entreeText.setText("");
		
		this.aSoumis = false;
		btnSoumettre = new JButton("Soumettre");
		btnSoumettre.setBounds(798, 486, 117, 29);
		frmTOHT.getContentPane().add(btnSoumettre);
		
		this.aChoisi  = false;
		
		this.btnTrickDeck = new JButton();
		this.btnTrickDeck.setBounds(813, 220, 147, 90);
		this.btnTrickDeck.setOpaque(false);
		this.btnTrickDeck.setContentAreaFilled(false);
		this.btnTrickDeck.setBorderPainted(false);
		frmTOHT.getContentPane().add(this.btnTrickDeck);
		this.btnTrickDeck.setVisible(false);
		
		this.btnTrickPile = new JButton();
		this.btnTrickPile.setBounds(813, 110, 147, 90);
		this.btnTrickPile.setOpaque(false);
		this.btnTrickPile.setContentAreaFilled(false);
		this.btnTrickPile.setBorderPainted(false);
		frmTOHT.getContentPane().add(this.btnTrickPile);
		this.btnTrickPile.setVisible(false);
		
		this.btnCdJ1 = new JButton();
		this.btnCdJ1.setBounds(140, 175, 90, 147);
		this.btnCdJ1.setOpaque(false);
		this.btnCdJ1.setContentAreaFilled(false);
		this.btnCdJ1.setBorderPainted(false);
		frmTOHT.getContentPane().add(this.btnCdJ1);
		this.btnCdJ1.setVisible(false);
		
		this.btnCgJ1 = new JButton();
		this.btnCgJ1.setBounds(30, 175, 90, 147);
		this.btnCgJ1.setOpaque(false);
		this.btnCgJ1.setContentAreaFilled(false);
		this.btnCgJ1.setBorderPainted(false);
		frmTOHT.getContentPane().add(this.btnCgJ1);
		this.btnCgJ1.setVisible(false);
		
		this.btnCdJ2 = new JButton();
		this.btnCdJ2.setBounds(410, 175, 90, 147);
		this.btnCdJ2.setOpaque(false);
		this.btnCdJ2.setContentAreaFilled(false);
		this.btnCdJ2.setBorderPainted(false);
		frmTOHT.getContentPane().add(this.btnCdJ2);
		this.btnCdJ2.setVisible(false);
		
		this.btnCgJ2 = new JButton();
		this.btnCgJ2.setBounds(300, 175, 90, 147);
		this.btnCgJ2.setOpaque(false);
		this.btnCgJ2.setContentAreaFilled(false);
		this.btnCgJ2.setBorderPainted(false);
		frmTOHT.getContentPane().add(this.btnCgJ2);
		this.btnCgJ2.setVisible(false);
		
		this.btnCdJ3 = new JButton();
		this.btnCdJ3.setBounds(680, 175, 90, 147);
		this.btnCdJ3.setOpaque(false);
		this.btnCdJ3.setContentAreaFilled(false);
		this.btnCdJ3.setBorderPainted(false);
		frmTOHT.getContentPane().add(this.btnCdJ3);
		this.btnCdJ3.setVisible(false);
		
		this.btnCgJ3 = new JButton();
		this.btnCgJ3.setBounds(570, 175, 90, 147);
		this.btnCgJ3.setOpaque(false);
		this.btnCgJ3.setContentAreaFilled(false);
		this.btnCgJ3.setBorderPainted(false);
		frmTOHT.getContentPane().add(this.btnCgJ3);
		this.btnCgJ3.setVisible(false);
		
		this.btnThProp = new JButton();
		this.btnThProp.setBounds(95, 3, 90, 147);
		this.btnThProp.setOpaque(false);
		this.btnThProp.setContentAreaFilled(false);
		this.btnThProp.setBorderPainted(false);
		frmTOHT.getContentPane().add(this.btnThProp);
		this.btnThProp.setVisible(false);
		
		this.CgJ1 = new JLabel();
		this.CgJ1.setBounds(30, 175, 90, 147);
		frmTOHT.getContentPane().add(this.CgJ1);
		
		this.CdJ1 = new JLabel();
		this.CdJ1.setBounds(140, 175, 90, 147);
		frmTOHT.getContentPane().add(this.CdJ1);
		
		this.CgJ2 = new JLabel();
		this.CgJ2.setBounds(300, 175, 90, 147);
		frmTOHT.getContentPane().add(this.CgJ2);
		
		this.CdJ2 = new JLabel();
		this.CdJ2.setBounds(410, 175, 90, 147);
		frmTOHT.getContentPane().add(this.CdJ2);
		
		this.CgJ3 = new JLabel();
		this.CgJ3.setBounds(570, 175, 90, 147);
		frmTOHT.getContentPane().add(this.CgJ3);
		
		this.CdJ3 = new JLabel();
		this.CdJ3.setBounds(680, 175, 90, 147);
		frmTOHT.getContentPane().add(this.CdJ3);
		
		this.lblTrickpile = new JLabel();
		this.lblTrickpile.setBounds(803, 110, 147, 90);
		frmTOHT.getContentPane().add(this.lblTrickpile);
		
		this.lblTrickdeck = new JLabel();
		this.lblTrickdeck.setBounds(803, 220, 147, 90);
		frmTOHT.getContentPane().add(this.lblTrickdeck);
		
		this.thProp = new JLabel();
		this.thProp.setBounds(95, 3, 90, 147);
		frmTOHT.getContentPane().add(this.thProp);
		
		this.lblJoueur_1 = new JLabel("Joueur 1");
		lblJoueur_1.setForeground(Color.WHITE);
		this.lblJoueur_1.setBounds(40, 330, 118, 16);
		frmTOHT.getContentPane().add(this.lblJoueur_1);
		
		JLabel lblScore_1 = new JLabel("Score :");
		lblScore_1.setForeground(Color.WHITE);
		lblScore_1.setBounds(160, 330, 56, 16);
		frmTOHT.getContentPane().add(lblScore_1);
		
		this.labelPoints_1 = new JLabel(""+0);
		labelPoints_1.setForeground(Color.WHITE);
		labelPoints_1.setBounds(208, 330, 42, 16);
		frmTOHT.getContentPane().add(labelPoints_1);
		
		this.lblJoueur_2 = new JLabel("Joueur 2");
		lblJoueur_2.setForeground(Color.WHITE);
		this.lblJoueur_2.setBounds(296, 331, 112, 16);
		frmTOHT.getContentPane().add(this.lblJoueur_2);
		
		JLabel labelScore_2 = new JLabel("Score :");
		labelScore_2.setForeground(Color.WHITE);
		labelScore_2.setBounds(409, 330, 56, 16);
		frmTOHT.getContentPane().add(labelScore_2);
		
		this.labelPoints_2 = new JLabel(""+0);
		labelPoints_2.setForeground(Color.WHITE);
		labelPoints_2.setBounds(458, 330, 42, 16);
		frmTOHT.getContentPane().add(labelPoints_2);
		
		this.lblJoueur_3 = new JLabel("Joueur 3");
		lblJoueur_3.setForeground(Color.WHITE);
		this.lblJoueur_3.setBounds(564, 331, 117, 16);
		frmTOHT.getContentPane().add(this.lblJoueur_3);
		
		JLabel labelScore_3 = new JLabel("Score :");
		labelScore_3.setForeground(Color.WHITE);
		labelScore_3.setBounds(683, 330, 56, 16);
		frmTOHT.getContentPane().add(labelScore_3);
		
		this.labelPoints_3 = new JLabel(""+0);
		labelPoints_3.setForeground(Color.WHITE);
		labelPoints_3.setBounds(728, 330, 42, 16);
		frmTOHT.getContentPane().add(labelPoints_3);
		
		JLabel lblthProp = new JLabel("7th Prop");
		lblthProp.setForeground(Color.WHITE);
		lblthProp.setBounds(12, 63, 56, 16);
		frmTOHT.getContentPane().add(lblthProp);
		
		this.btnConsole = new JButton("");
		btnConsole.setBounds(380, 6, 200, 90);
		frmTOHT.getContentPane().add(btnConsole);
		this.btnConsole.setOpaque(false);
		this.btnConsole.setContentAreaFilled(false);
		this.btnConsole.setBorderPainted(false);
		
		this.backCard = new ImageIcon(this.urlCardBack);
		this.backCardRotated = new ImageIcon(this.urlCardBackRotated);
		
	}
	
	public static void main(String[] args)  throws IOException{

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VueConsole vc = new VueConsole();
					VueGraphique vg = new VueGraphique();
					vg.frmTOHT.setVisible(true);
					Partie p = new Partie(vc, vg);
					ControleurLO02 c = new ControleurLO02(vg.btnSoumettre, vg.btnFinPartie, vg.btnTrickDeck, vg.btnTrickPile, 
							vg.btnCdJ1, vg.btnCgJ1, vg.btnCdJ2, vg.btnCgJ2, vg.btnCdJ3, vg.btnCgJ3, vg.btnThProp, vg.btnConsole, p, vg);
					Thread t = new Thread(p);
					t.start();
					vg.constructionControleur();
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}	
	public void constructionControleur() {

	}
	
public void update(Observable o, Object arg) {
		
		if(o instanceof Partie) {
			
			switch((Evenement)arg) {
			
			case BIENVENUE:
				this.ta.setText("----------Bienvenue dans The Other Hat Trick Virtual Game----------\n");
				try{Thread.sleep(2000);}
				catch(Exception e) {e.printStackTrace();}
				break;
			case CHOIX_REGLES :
				int option = this.dBox.showConfirmDialog(null, "Voulez vous jouer avec les règles par défaut ? Si non vous pourrez configurer vous même votre partie", "Configuration de la partie", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if(option == JOptionPane.OK_OPTION){
					
					p.setChoix(1);
					
					}
				else {

					p.setChoix(2);
					
				}
				break;
				
			case CB_JOUEURS :
				this.ta.setText("Combien de joueurs réels joueront cette partie :\n");
				
				this.soumettreChoix(p.getChoixVueConsole());
				
				break;
				
			case ERREUR_NB_JOUEURS : 
				this.ta.setText("Le nombre de joueurs entré est invalide, le nombre de joueur maximum est 3.\nCombien de joueurs réels joueront cette partie :\n");
				
				this.soumettreChoix(p.getChoixVueConsole());

				break;
			case INDICE_SAISIE_JOUEUR :
				this.ta.setText("Saisie des informations du joueur "+(this.interInt+1)+":\n");
				break;
			case DEBUT_PARTIE :
				this.ta.append("\nDébut de la partie ...\n");
				this.attribuerNoms();
				this.actualiserPlateau();
				break;
			case DEBUT_TOUR :
				this.ta.setText("------------------------------\n");	
				
				if(this.p.getJoueurActuel() == 0) {
					this.lblJoueur_1.setForeground(Color.RED);
					this.lblJoueur_2.setForeground(Color.WHITE);
					this.lblJoueur_3.setForeground(Color.WHITE);
				}
				else if(this.p.getJoueurActuel() == 1) {
					this.lblJoueur_2.setForeground(Color.RED);
					this.lblJoueur_1.setForeground(Color.WHITE);
					this.lblJoueur_3.setForeground(Color.WHITE);
				}
				else {
					this.lblJoueur_3.setForeground(Color.RED);
					this.lblJoueur_2.setForeground(Color.WHITE);
					this.lblJoueur_1.setForeground(Color.WHITE);
				}
				
				this.ta.append("Au tour de " + this.interString + " de jouer !\n");
				this.actualiserPlateau();
				break;
				
				//Gérer les erreurs de saisie dans les cases afin de passer directement une valeur correcte au moteur du jeu
				
			/*case ERREUR_CHOIX_1_2 :
				System.out.println("Entrez une valeur correcte (Entre 1 et 2)");
				break;
			case ERREUR_CHOIX_1_3 :
				System.out.println("Entrez une valeur correcte (Entre 1 et 3)");
				break;*/
				
			case VAINQUEUR :
				
				this.labelPoints_1.setText(""+this.interJTab[0].getPoints());
				this.labelPoints_2.setText(""+this.interJTab[1].getPoints());
				this.labelPoints_3.setText(""+this.interJTab[2].getPoints());
				
				this.ta.setText("Partie terminée ! Le vainqueur est ........");
				this.ta.append(this.interJoueur.getNom()+" avec "+this.interJoueur.getPoints()+" points."+"\nFélicitations ! vous avez gagné le droit de rejouer\n");
				this.ta.append("\n\nDéveloppé par Corentin Dacal et Clément Baptista");
				break;
			case ANNONCE_3_TOURS_TOHT :
				this.ta.append("Aie... The Other Hat Trick est en jeu depuis 3 tours... Il est temps de mettre un terme à la partie....\n");
				break;
			case RETRANCHER_MALUS:
				this.ta.append(this.interJoueur.getNom()+" vous avez la carte "+this.interString+" qui comporte un malus ... vous perdez "+this.interInt+" points...");
				this.ta.append("Vous avez maintenant "+this.interJoueur.getPoints()+" points.");
				
				break;
			case CHOIX_SEVENTH_PROP :
				
				//à faire avec les cartes directement
				
				
				this.ta.setText("Voulez-vous échanger une de vos cartes avec le 7th prop ? Clickez qur la carte que vous souhaitez placer à la place du 7th prop \n");
				this.btnThProp.setVisible(true);
				this.p.getSeventhProp().montrer();
				this.actualiserPlateau();
				
				if(this.p.getJoueurActuel() == 0) {
					this.btnCdJ1.setVisible(true);
					this.btnCgJ1.setVisible(true);
				}
				else if(this.p.getJoueurActuel() == 1) {
					this.btnCdJ2.setVisible(true);
					this.btnCgJ2.setVisible(true);
				}
				else {
					this.btnCdJ3.setVisible(true);
					this.btnCgJ3.setVisible(true);
				}
				
				this.attendreChoixCarte(p.getChoixVueConsole());
				
				
				this.p.getSeventhProp().cacher();
				this.btnCdJ1.setVisible(false);
				this.btnCgJ1.setVisible(false);
				this.btnCdJ2.setVisible(false);
				this.btnCgJ2.setVisible(false);
				this.btnCdJ3.setVisible(false);
				this.btnCgJ3.setVisible(false);
				this.btnThProp.setVisible(false);
				
				this.setAChoisi(false);
				
				break;
			case MELANGER_CARTES_SEVENTH_PROP :
				this.actualiserPlateau();
				this.ta.append("Voici vos deux nouveaux props");
		        
				if(!p.getChoixVueConsole()) {
					
			        option = this.dBox.showConfirmDialog(null, "Souhaitez vous échanger la position de ces 2 cartes dans votre main afin de brouiller les pistes ?\n", "Échanger les props ?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if(option == JOptionPane.OK_OPTION){
						
						p.setChoix(1);
						
					}
					else {
						
						p.setChoix(2);
						
					}
				
				}	
					
				this.actualiserPlateau();
				break;
			}
			
		}
		
		else if(o instanceof Trick) {
			
			switch((Evenement)arg){
			
			case TRICK_REALISABLE :
				this.ta.append("Vous avez les props nécessaires pour réaliser ce trick !\n");
				break;
			case TRICK_NON_REALISABLE :
				this.ta.append("Vous n'avez malheureseument pas les props nécessaires pour réaliser ce trick... Vous devez donc retourner une de vos cartes face cachée. \n");
				break;
			default :
				break;
			
			}
			
		}
		
		else if(o instanceof Joueur) {
			switch((Evenement)arg) {
			
			case SAISIE_NOM_jOUEUR:
				this.ta.append("Entrer votre nom : \n");
				
				this.soumettreChoixText(p.getChoixVueConsole());
				
				break;
			case ERREUR_SAISIE_NOM:
				this.ta.append("Entrez un nom qui n'est pas vide\n");
				
				this.soumettreChoixText(p.getChoixVueConsole());
				
				break;
			case SAISIE_DATE_NAISSANCE:
				this.ta.append("Entrez votre date de naissance (JJ/MM/AAAA) :\n");
				
				this.soumettreChoixText(p.getChoixVueConsole());
				
				break;
			case ERREUR_FORMAT_DATE:
				this.ta.append("Erreur format, entrer une date au bon format (dd/MM/yyyy) :");
				break;
			case AFFICHAGE_POINTS:
				this.ta.append("Ta-Dah ! Le compteur de points monte à "+this.interInt+" !\n");
				
				if(this.p.getJoueurActuel() == 0) {
					this.labelPoints_1.setText(""+this.interInt);;
				}
				else if(this.p.getJoueurActuel() == 1) {
					this.labelPoints_2.setText(""+this.interInt);;
				}
				else {
					this.labelPoints_3.setText(""+this.interInt);;
				}
				
				break;
			}
		}
		else if(o instanceof StrategyVirtuelle) {
			switch((Evenement)arg) {
			
			case PREPARATION_PROPS_JVIRTUEL:
				this.ta.append(this.interString + " prépare ses props\n");
				this.actualiserPlateau();
				break;
			case TRICK_A_REALISER:
				
				//Optionnel je pense car on a déja le trick en graphique
				
				//this.ta.append("Le trick à  réaliser est " + this.interString+"\n");
				break;
			case PROPS_OK:
				this.ta.append(this.interString + "  a les props nécessaires pour realiser son trick !");
				break;
			case PROPS_NOT_OK:
				this.ta.append(this.interString + " n'a malheureseument pas les props nécessaires pour realiser son trick... Il donc retourner une de ses cartes face cachée. \n");
				break;
			case RETOURNER_CARTE_DECK:
				this.ta.append(this.interString + " choisi de retourner une carte du deck\n");
				this.actualiserPlateau();
				//actualiser affichage du Trick pile
				break;
			case RETOURNER_CARTE_PILE:
				this.ta.append(this.interString + " choisi le trick du dessus de la pile\n");
				break;
			case ERREUR_DECK_VIDE:
				this.ta.append(this.interString+" ne peut pas retourner un autre trick car le trick deck est vide...\n");
				break;
			case RETOURNER_CARTE_DROITE:
				
				//actualiser les cartes des joueurs
				
				//System.out.println(this.interJoueur.getNom() + " retourne sa carte droite: "+this.interCarteDroite.getNom());
				break;
			case RETOURNER_CARTE_GAUCHE:
				
				//actualiser les cartes des joueurs
				
				//System.out.println(this.interJoueur.getNom() + " retourne sa carte gauche: "+this.interCarteGauche.getNom());
				break;

			case REUSSIR_TRICK:
				this.actualiserPlateau();
				break;
			}
		}
		
		else if(o instanceof StrategyReelle) {
			switch((Evenement)arg) {
			
			case ACTUALISER_PLATEAU:
				
				this.actualiserPlateau();
				
				break;
			
			case TRICK_INITIAL :
				
				//Idem, potentiellement inutile à part si on voit mal
				
				//System.out.println("\nLa carte du dessus de la trick pile est " + this.interString);
				break;
			case CHOIX_ACTION:
				
				this.ta.append("Voulez-vous réaliser ce trick ou voulez-vous en piocher un autre ? Cliquez sur le trickdeck ou la trickpile en fonction de votre choix.\n");
				
				//Faire une action pour choisir le trick, par exemple cliquer sur le trickdeck pour piocher
				this.btnTrickDeck.setVisible(true);
				this.btnTrickPile.setVisible(true);
				
				this.attendreChoixCarte(p.getChoixVueConsole());
				
				this.btnTrickDeck.setVisible(false);
				this.btnTrickPile.setVisible(false);
				this.setAChoisi(false);
				//System.out.println("\n1. Voulez-vous réaliser ce trick\n2.  Voulez-vous retourner une carte du trick deck\n");
				break;
			case ERREUR_CHOIX_1_2:
				
				//Pas d'erreur car on va gérer la bonne saisie dans les case
				
				//System.out.println("Entrez une valeur correcte (Entre 1 et 2)");
				break;
			case ERREUR_DECK_VIDE:
				this.ta.append("\nVous ne pouvez pas retourner un autre trick car le trick deck est vide...\n");
				break;
			case DEMANDE_CARTE:
				this.ta.append("Quelle carte voulez-vous prendre ? Cliquez sur la carte d'un autre joueur que vous voulez prendre.\n");
				
				if(this.p.getJoueurActuel() == 0) {
					this.btnCdJ2.setVisible(true);
					this.btnCgJ2.setVisible(true);
					this.btnCdJ3.setVisible(true);
					this.btnCgJ3.setVisible(true);
				}
				else if(this.p.getJoueurActuel() == 1) {
					this.btnCdJ1.setVisible(true);
					this.btnCgJ1.setVisible(true);
					this.btnCdJ3.setVisible(true);
					this.btnCgJ3.setVisible(true);
				}
				else {
					this.btnCdJ1.setVisible(true);
					this.btnCgJ1.setVisible(true);
					this.btnCdJ2.setVisible(true);
					this.btnCgJ2.setVisible(true);
				}
				
				this.attendreChoixCarte(p.getChoixVueConsole());
				
				this.setAChoisi(false);
				
				this.btnCdJ1.setVisible(false);
				this.btnCgJ1.setVisible(false);
				this.btnCdJ2.setVisible(false);
				this.btnCgJ2.setVisible(false);
				this.btnCdJ3.setVisible(false);
				this.btnCgJ3.setVisible(false);
				break;
			case ERREUR_CHOIX_1_4:
				
				//Idem, pas d'erreur
				
				//System.out.println("Entrez une valeur correcte (Entre 1 et 4)");
				break;
			case ECHANGER_CARTE:
				
				this.actualiserPlateau();
				
				this.ta.append("Choisissez votre carte à échanger contre celle que vous venez de choisir. Cliquez sur celle que vous ne voulez plus.\n");
				
				if(this.p.getJoueurActuel() == 0) {
					this.btnCdJ1.setVisible(true);
					this.btnCgJ1.setVisible(true);
				}
				else if(this.p.getJoueurActuel() == 1) {
					this.btnCdJ2.setVisible(true);
					this.btnCgJ2.setVisible(true);
				}
				else {
					this.btnCdJ3.setVisible(true);
					this.btnCgJ3.setVisible(true);
				}
				
				this.attendreChoixCarte(p.getChoixVueConsole());
				
				this.btnCdJ1.setVisible(false);
				this.btnCgJ1.setVisible(false);
				this.btnCdJ2.setVisible(false);
				this.btnCgJ2.setVisible(false);
				this.btnCdJ3.setVisible(false);
				this.btnCgJ3.setVisible(false);
				
				this.setAChoisi(false);
				break;
			case CHOIX_EN_PLUS:
				this.ta.setText("Oh... On dirait qu'il vous reste encore "+this.interInt+" chances !\n");
				
				if(!p.getChoixVueConsole()) {
					int option = this.dBox.showConfirmDialog(null, "Voulez vous utiliser une de vos chances supplémentaires ?", "Chance supplémentaire ?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if(option == JOptionPane.OK_OPTION){
						p.setChoix(1);
					}
					else {
						p.setChoix(2);
					}								
				}
				break;
			case OBLIGATION_RETOURNER_CARTE_GAUCHE:
				this.ta.append("Vous retournez votre carte gauche car votre carte droite est déjà retournée");
				break;
			case OBLIGATION_RETOURNER_CARTE_DROITE:
				this.ta.append("Vous retournez votre carte droite car votre carte gauche est déjà retournée");
				break;
			case CHOIX_CARTE_RETOURNER :
				this.ta.append("Choisissez la carte à retourner. Pour ce faire cliquez sur la carte voulue");
				
				if(this.p.getJoueurActuel() == 0) {
					this.btnCdJ1.setVisible(true);
					this.btnCgJ1.setVisible(true);
				}
				else if(this.p.getJoueurActuel() == 1) {
					this.btnCdJ2.setVisible(true);
					this.btnCgJ2.setVisible(true);
				}
				else {
					this.btnCdJ3.setVisible(true);
					this.btnCgJ3.setVisible(true);
				}
				
				this.attendreChoixCarte(p.getChoixVueConsole());
				
				this.btnCdJ1.setVisible(false);
				this.btnCgJ1.setVisible(false);
				this.btnCdJ2.setVisible(false);
				this.btnCgJ2.setVisible(false);
				this.btnCdJ3.setVisible(false);
				this.btnCgJ3.setVisible(false);
				
				this.setAChoisi(false);
				break;
			case TRICK_A_REALISER :
				
				//Déjà dans l'interface graphique
				
				//System.out.println("Le trick à réaliser est " + this.interString);
				break;
			case CHOIX_REAL_TRICK:
				this.ta.append("Souhaitez-vous réaliser ce trick ? :\n1. Oui\n2. Non");
				if(!this.p.getChoixVueConsole()) {
					
				
					int option = this.dBox.showConfirmDialog(null, "Souhaitez-vous réaliser ce trick ?", "TADAAAA ?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if(option == JOptionPane.OK_OPTION){
						
						p.setChoix(1);
						
					}
					else {
						
						p.setChoix(2);
						
					}
				}
					break;
					
				}
		}
		
		else if(o instanceof ChoixExtension) {
			
			switch((Evenement)arg){
			
			case CHOIX_EXTENSION :
				if(!p.getChoixVueConsole()) {
					int option = this.dBox.showConfirmDialog(null, "Voulez-vous jouer avec le jeu de cartes par défaut, ou voulez-vous jouer avec une variante sur le thème de la gastronomie américaine ?", "Choix extension", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if(option == JOptionPane.OK_OPTION){
						
							p.setChoix(1);
						
						}
					else {
						
						p.setChoix(2);
						
					}
				}
				break;
				
			default :
				break;
			
			}
			
		}
		
else if(o instanceof PartiePersonalisee) {
			
			switch((Evenement)arg){
			
			case CONFIGURATEUR_PARTIE :
				if(!p.getChoixVueConsole()) {
					int option = this.dBox.showConfirmDialog(null, "nombre maximum de tours ?", "tada", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if(option == JOptionPane.OK_OPTION){
						this.p.setChoix(1);
					}
					else {
						this.p.setChoix(2);				
					}
				}
				break;
				
			case CHOIX_NB_TOURS_MAX :
				String interChoix ="";
					
				this.ta.setText("Combien de tours maximum voulez vous jouer ? (Entre 1 et 50)");
				
				this.soumettreChoix(p.getChoixVueConsole());
				break;
				
			case ERREUR_CHOIX_1_50:
				this.ta.setText("Entrez une valeur correcte (Entre 1 et 50)");
				
				this.soumettreChoix(p.getChoixVueConsole());
				break;
				
			case CHANCES_SUP :
				
				this.ta.setText("Voulez-vous jouer avec des chances en plus lors de la préparation des props ? (Entre 0 et 2)");
				
				this.soumettreChoix(p.getChoixVueConsole());
				break;
				
			case ERREUR_CHOIX_1_2:
				
				this.ta.setText("Entrer une valeur correcte (Entre 0 et 2)");
				
				this.soumettreChoix(p.getChoixVueConsole());
				break;
				
			default :
				break;
			
			}
			
		}
		
	}

	public String recupererText() {
		
		String recup = this.entreeText.getText();
		
		return recup;
		
	}
	
	public void setASoumis(Boolean b) {
		
		this.aSoumis = b;
		
	}
	
	public Boolean getASoumis() {
		
		return this.aSoumis;
		
	}
	
	public void setAChoisi(Boolean b) {
		
		this.aChoisi = b;
		
	}
	
	public Boolean getAChoisi() {
		
		return this.aChoisi;
		
	}
	
	public void setChoix(int c) {
		this.choix = c;
	}
	
	public int getChoix() {
		return this.choix;
	}
	
	public void setInterInt(int i) {
		
		this.interInt = i;
		
	}
	
	public void setChoixEchange(int c) {
		this.choixEchange = c;
	}
	
	public int getChoixEchange() {
		return this.choixEchange;
	}
	
	public void setInterString(String chaine) {
		
		this.interString = chaine;
		
	}
	
	public void setInterJoueur(Joueur j) {
		
		this.interJoueur = j;
		
	}
	
	public void setInterCarteGauche(Prop carteGauche) {
		
		this.interCarteGauche = carteGauche;
		
	}
	
	public void setInterCarteDroite(Prop carteDroite) {
		
		this.interCarteDroite = carteDroite;
		
	}
	
	public void setPartie(Partie p) {
		this.p = p;
	}
	
	public Partie getPartie() {
		return this.p;
	}

	
	public void montrerCarteJoueurActuel() {
		// Au debut du tour d'un joueur, montre ses deux cartes pour qu'il puisse jouer
			if(this.p.getJoueurActuel() == 0) {
				if(this.interJTab[this.p.getJoueurActuel()].getCarteDroite().getEstVisible()) {
					
				}
				else {
					this.CdJ1.setIcon(new ImageIcon(this.interJTab[this.p.getJoueurActuel()].getCarteDroite().getLienImage()));
				}
				if(this.interJTab[this.p.getJoueurActuel()].getCarteGauche().getEstVisible()) {
					
				}
				else {
					this.CgJ1.setIcon(new ImageIcon(this.interJTab[this.p.getJoueurActuel()].getCarteGauche().getLienImage()));
				}
			}
			else if(this.p.getJoueurActuel() == 1) {
				if(this.interJTab[this.p.getJoueurActuel()].getCarteDroite().getEstVisible()) {
					
				}
				else {
					this.CdJ2.setIcon(new ImageIcon(this.interJTab[this.p.getJoueurActuel()].getCarteDroite().getLienImage()));
				}
				if(this.interJTab[this.p.getJoueurActuel()].getCarteGauche().getEstVisible()) {
					
				}
				else {
					this.CgJ2.setIcon(new ImageIcon(this.interJTab[this.p.getJoueurActuel()].getCarteGauche().getLienImage()));
				}
			}
			else if(this.p.getJoueurActuel() == 2) {
				if(this.interJTab[this.p.getJoueurActuel()].getCarteDroite().getEstVisible()) {
					
				}
				else {
					this.CdJ3.setIcon(new ImageIcon(this.interJTab[this.p.getJoueurActuel()].getCarteDroite().getLienImage()));
				}
				if(this.interJTab[this.p.getJoueurActuel()].getCarteGauche().getEstVisible()) {
					
				}
				else {
					this.CgJ3.setIcon(new ImageIcon(this.interJTab[this.p.getJoueurActuel()].getCarteGauche().getLienImage()));
				}
			}
			
		}
		
		public void actualiserPlateau() {
			
			// Ã  chaque appel, actualise graphiquement les cartes de chaque joueur. Ex: aprÃ¨s qu'un joueur ait prÃ©parÃ© ses props, on actualise le pateau afin qu'il voit qu'il a Ã©changÃ© une carte
			if(this.p.getSeventhProp().getEstVisible()) {
				this.thProp.setIcon(new ImageIcon(this.p.getSeventhProp().getLienImage()));
			}
			else {
				this.thProp.setIcon(this.backCard);
			}
			
			for(int i = 0; i < 3; i++) {
				if(i == 0) {
					if(this.interJTab[i].getCarteDroite().getEstVisible()) {
						this.CdJ1.setIcon(new ImageIcon(this.interJTab[i].getCarteDroite().getLienImage()));
					}
					else
						this.CdJ1.setIcon(backCard);
					if(this.interJTab[i].getCarteGauche().getEstVisible()) {
						this.CgJ1.setIcon(new ImageIcon(this.interJTab[i].getCarteGauche().getLienImage()));
					}
					else
						this.CgJ1.setIcon(backCard);
				}
				else if(i == 1) {
					if(this.interJTab[i].getCarteDroite().getEstVisible()) {
						this.CdJ2.setIcon(new ImageIcon(this.interJTab[i].getCarteDroite().getLienImage()));
					}
					else
						this.CdJ2.setIcon(backCard);
					if(this.interJTab[i].getCarteGauche().getEstVisible()) {
						this.CgJ2.setIcon(new ImageIcon(this.interJTab[i].getCarteGauche().getLienImage()));
					}
					else
						this.CgJ2.setIcon(backCard);
				}
				else if(i == 2) {
					if(this.interJTab[i].getCarteDroite().getEstVisible()) {
						this.CdJ3.setIcon(new ImageIcon(this.interJTab[i].getCarteDroite().getLienImage()));
					}
					else
						this.CdJ3.setIcon(backCard);
					if(this.interJTab[i].getCarteGauche().getEstVisible()) {
						this.CgJ3.setIcon(new ImageIcon(this.interJTab[i].getCarteGauche().getLienImage()));
					}
					else
						this.CgJ3.setIcon(backCard);
				}
			}
			this.montrerCarteJoueurActuel();
			this.actualiserTrick();
		}
		
		public void actualiserTrick() {
			// permet d'actualiser le trick deck et le trick pile aprÃ¨s une pioche. Si le trick deck est vide alors on enlÃ¨ve l'affichage du label. Le label du trick deck correspond juste Ã  notre dos de carte
			if(this.p.getDeckContent()) {
				this.lblTrickdeck.setVisible(false);
			}
			else {
				this.lblTrickdeck.setIcon(this.backCardRotated);
			}

			this.lblTrickpile.setIcon(new ImageIcon(this.p.getFirstPileCard().getLienImage()));
			
		}
		
		public void attribuerNoms() {
			this.lblJoueur_1.setText(this.interJTab[0].getNom());
			this.lblJoueur_2.setText(this.interJTab[1].getNom());
			this.lblJoueur_3.setText(this.interJTab[2].getNom());
			
		}

	public Joueur[] getInterJTab() {
		return interJTab;
	}

	public void setInterJTab(Joueur[] interJTab) {
		this.interJTab = interJTab;
	}
	
	public void setStratR(StrategyReelle s) {
		this.stratR = s;
	}
	
	public void soumettreChoix(boolean choixVue) {
		
		
		if(!choixVue) {
			
			while(!this.getASoumis()) {
				
				try{Thread.sleep(100);}
				catch(InterruptedException e) {
					
					e.printStackTrace();
					
				}
				
			}
			
			String interChoix = this.recupererText();
			int choix = Integer.parseInt(interChoix);
			this.p.setChoix(choix);
			
			this.setASoumis(false);
			this.entreeText.setText("");
			
		}
		
		
	}
	
public void soumettreChoixEchange(boolean choixVue) {
		
		
		if(!choixVue) {
			
			while(!this.getASoumis()) {
				
				try{Thread.sleep(100);}
				catch(InterruptedException e) {
					
					e.printStackTrace();
					
				}
				
			}
			
			String interChoixC = this.recupererText();
			int choixC = Integer.parseInt(interChoixC);
			this.p.setChoixEchange(choixC);
			
		}
		
		
	}

public void soumettreChoixText(boolean choixVue) {
	
	if(!choixVue) {
		
		while(!this.getASoumis()) {
			
			try{Thread.sleep(100);}
			catch(InterruptedException e) {
				
				e.printStackTrace();
				
			}
			
		}
		
		this.interJoueur.setTestString(this.recupererText());
		this.setASoumis(false);
		this.entreeText.setText("");
		
	}
	
}

public void attendreChoixCarte(boolean vueConsole) {
	
if(!vueConsole) {
	while(!this.getAChoisi()) {
		
		try{Thread.sleep(100);}
		catch(InterruptedException e) {
			
			e.printStackTrace();
			
			}
		
		}

	}

}
}
