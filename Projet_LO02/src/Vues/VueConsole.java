package Vues;

import projet_LO02.*;
import java.util.Observable;
import java.util.Observer;

public class VueConsole implements Observer{
	
	private int interInt;
	private String interString;
	private Joueur interJoueur;
	private Prop interCarteGauche;
	private Prop interCarteDroite;
	
	public VueConsole() {
		
		
		
	}
	
	public void update(Observable o, Object arg) {
		
		if(o instanceof Partie) {
			
			switch((Evenement)arg) {
			
			case BIENVENUE:
				System.out.println("----------Bienvenue dans The Other Hat Trick Virtual Game----------\n");
				break;
			case CB_JOUEURS :
				System.out.println("Combien de joueurs réels joueront cette partie :\n");
				break;
			case ERREUR_NB_JOUEURS : 
				System.out.println("Le nombre de joueurs entré est invalide, le nombre de joueur maximum est 3.\n");
				break;
			case INDICE_SAISIE_JOUEUR :
				System.out.println("Saisie des informations du joueur "+(this.interInt+1)+":\n");
				break;
			case DEBUT_PARTIE :
				System.out.println("\nDébut de la partie ...\n");
				break;
			case DEBUT_TOUR :
				System.out.println("------------------------------\n");	
				System.out.println("Au tour de " + this.interString + " de jouer !\n");
				break;
			case CHANGER_DE_VUE :
				System.out.println("Voulez vous passer sur la vue graphique ?\n1. Non\n2. Oui");
				break;
			case ERREUR_CHOIX_1_2 :
				System.out.println("Entrez une valeur correcte (Entre 1 et 2)");
				break;
			case ERREUR_CHOIX_1_3 :
				System.out.println("Entrez une valeur correcte (Entre 1 et 3)");
				break;
			case VAINQUEUR :
				System.out.println("Partie termin�e ! Le vainqueur est ........");
				System.out.println(this.interJoueur.getNom()+" avec "+this.interJoueur.getPoints()+" points."+"\nFélicitations ! vous avez gagner le droit de rejouer\n");
				System.out.println("\n\nDéveloppé par Corentin Dacal et Clément Baptista");
				break;
			case ANNONCE_3_TOURS_TOHT :
				System.out.println("Aie... The Other Hat Trick est en jeu depuis 3 tours... Il est temps de mettre un terme à la partie....\n");
				break;
			case RETRANCHER_MALUS:
				System.out.println(this.interJoueur.getNom()+" vous avez la carte "+this.interString+" qui comporte un malus ... vous perdez "+this.interInt+" points...");
				System.out.println("Vous avez maintenant "+this.interJoueur.getPoints()+" points.");
				break;
			case CHOIX_SEVENTH_PROP :
				System.out.println("Comme vous avez réussi le trick, vous pouvez échanger une de vos cartes avec le Seventh Prop tout en recachant vos cartes visibles.\n");
				System.out.println("Votre carte gauche est : " + this.interCarteGauche.getNom() + "\nVotre carte droite est : " + this.interCarteDroite.getNom());
				System.out.println("Vous prenez le seventh prop en main, ce dernier est : " + this.interString);
				System.out.println("Quelle carte souhaitez vous remettre au milieu de la table en tant que seventh prop ?\n1. " + this.interCarteGauche.getNom() +"\n2. " + this.interCarteDroite.getNom() + "\n3. " + this.interString);
				break;
			case MELANGER_CARTES_SEVENTH_PROP :
				System.out.println("Maintenant, suite à votre d�cision, votre carte gauche est : " + this.interCarteGauche.getNom() + "\nVotre carte droite est : " + this.interCarteDroite.getNom());
		        System.out.println("Souhaitez vous échanger la position de ces 2 cartes dans votre main afin de brouiller les pistes ?\n1. Oui\n2. Non");
				break;
			case CHOIX_REGLES:
				System.out.println("Voulez vous :\n1. Jouer avec les règles par défaut\n2. Configurer vous-même votre partie");
				break;
			}
			
		}
		else if(o instanceof PartiePersonalisee) {
			
			switch((Evenement)arg) {
			
			case CONFIGURATEUR_PARTIE :
				System.out.println("Bienvenue dans le configurateur de partie !");
				System.out.println("Voulez vous jouer avec un nombre maximum de tours ? \n1. Oui\n2. Non");
				break;
			case CHOIX_NB_TOURS_MAX :
				System.out.println("Combien de tours maximum voulez-vous jouer ?(Entre 1 et 50)");
				break;
			case CHANCES_SUP:
				System.out.println("Combien de chances supplémentaires voulez-vous afin de préparer vos props ?(Entre 0 et 2)");
				break;
			case ERREUR_CHOIX_1_2 :
				System.out.println("Entrez une valeur correcte (Entre 1 et 2)");
				break;
			case ERREUR_CHOIX_1_50:
				System.out.println("Entrez une valeur correcte (Entre 1 et 50)");
				break;
			
			
			}
			
		}
		else if(o instanceof Trick) {
			
			switch((Evenement)arg){
			
			case TRICK_REALISABLE :
				System.out.println("Vous avez les props nécessaires pour réaliser ce trick !\n");
				break;
			case TRICK_NON_REALISABLE :
				System.out.println("Vous n'avez malheureseument pas les props nécessaires pour réaliser ce trick... Vous devez donc retourner une de vos cartes face cachée. \n");
				break;
			default :
				break;
			
			}
			
		}
		
		else if(o instanceof Joueur) {
			switch((Evenement)arg) {
			
			case SAISIE_NOM_jOUEUR:
				System.out.println("Entrer votre nom : ");
				break;
			case ERREUR_SAISIE_NOM:
				System.out.println("Entrez un nom qui n'est pas vide");
				break;
			case SAISIE_DATE_NAISSANCE:
				System.out.println("Entrez votre date de naissance (JJ/MM/AAAA) :");
				break;
			case ERREUR_FORMAT_DATE:
				System.out.println("Erreur format, entrer une date au bon format (dd/MM/yyyy) :");
				break;
			case AFFICHAGE_POINTS:
				System.out.println("Ta-Dah ! Le compteur de point monte à "+this.interInt+"  !\n");
				break;
			}
		}
		else if(o instanceof StrategyVirtuelle) {
			switch((Evenement)arg) {
			
			case PREPARATION_PROPS_JVIRTUEL:
				System.out.println(this.interString + " prépare ses props\n");
				break;
			case TRICK_A_REALISER:
				System.out.println("Le trick à réaliser est " + this.interString+"\n");
				break;
			case PROPS_OK:
				System.out.println(this.interString + "  a les props nécessaires pour realiser son trick !");
				break;
			case PROPS_NOT_OK:
				System.out.println(this.interString + " n'a malheureseument pas les props nécessaires pour realiser son trick... Il donc retourner une de ses cartes face cachée. ");
				break;
			case RETOURNER_CARTE_DECK:
				System.out.println(this.interString + " choisi de retourner une carte du deck");
				break;
			case RETOURNER_CARTE_PILE:
				System.out.println(this.interString + " choisi le trick du dessus de la pile");
				break;
			case ERREUR_DECK_VIDE:
				System.out.println(this.interString+" ne peut pas retourner un autre trick car le trick deck est vide...\n");
				break;
			case RETOURNER_CARTE_DROITE:
				System.out.println(this.interJoueur.getNom() + " retourne sa carte droite: "+this.interCarteDroite.getNom());
				break;
			case RETOURNER_CARTE_GAUCHE:
				System.out.println(this.interJoueur.getNom() + " retourne sa carte gauche: "+this.interCarteGauche.getNom());
				break;
			case CARTES_DEJA_RETOURNEES:
				System.out.println("Ses deux cartes sont déjà  retournées...");
				break;
			}
		}
		else if(o instanceof StrategyReelle) {
			switch((Evenement)arg) {
			
			case DESCRIPTION_PROPS:
				System.out.println("Votre carte gauche est : " + this.interCarteGauche.getNom() + "\nVotre carte droite est : " + this.interCarteDroite.getNom());
				break;
			case TRICK_INITIAL :
				System.out.println("\nLa carte du dessus de la trick pile est " + this.interString);
				break;
			case CHOIX_ACTION:
				System.out.println("\n1. Voulez-vous réaliser ce trick\n2.  Voulez-vous retourner une carte du trick deck\n");
				break;
			case ERREUR_CHOIX_1_2:
				System.out.println("Entrez une valeur correcte (Entre 1 et 2)");
				break;
			case CARTE_DESSUS_PILE:
				System.out.println("Mainteanant la carte du dessus de la trick pile est " + this.interString+"\n");
				break;
			case ERREUR_DECK_VIDE:
				System.out.println("\nVous ne pouvez pas retourner un autre trick car le trick deck est vide...\n");
				break;
			case CARTES_SUR_LE_PLATEAU:
				System.out.println("Les cartes sur le plateau sont : \n ");
				break;
			case CARTES_DU_JOUEUR:
				System.out.println("Voici les cartes de "+this.interJoueur.getNom()+": \n");
				break;
			case DESCRIPTION_CARTE_GAUCHE:
				System.out.println("\n1. Sa carte gauche de " + this.interJoueur.getNom() + " est : " + this.interJoueur.getCarteGauche().getNom());
				break;
			case CARTE_GAUCHE_CACHEE:
				System.out.println("\n1. Sa carte gauche est face cachée");
				break;
			case DESCRIPTION_CARTE_DROITE:
				System.out.println("\n2. Sa carte droite de " + this.interJoueur.getNom()+ " est : " + this.interJoueur.getCarteDroite().getNom()+"\n");
				break;
			case CARTE_DROITE_CACHEE:
				System.out.println("\n2. Sa carte droite est face cachée\n");
				break;
			case DESCRIPTION_CARTE_GAUCHE_2:
				System.out.println("\n3. Sa carte gauche de " + this.interJoueur.getNom() + " est : " + this.interJoueur.getCarteGauche().getNom());
				break;
			case CARTE_GAUCHE_CACHEE_2:
				System.out.println("\n3. Sa carte gauche est face cachée");
				break;
			case DESCRIPTION_CARTE_DROITE_2:
				System.out.println("\n4. Sa carte droite de " + this.interJoueur.getNom()+ " est : " + this.interJoueur.getCarteDroite().getNom()+"\n");
				break;
			case CARTE_DROITE_CACHEE_2:
				System.out.println("\n4. Sa carte droite est face cachée\n");
				break;
			case DEMANDE_CARTE:
				System.out.println("Quelle carte voulez-vous prendre ?");
				break;
			case ERREUR_CHOIX_1_4:
				System.out.println("Entrez une valeur correcte (Entre 1 et 4)");
				break;
			case ECHANGER_CARTE:
				System.out.println("Votre carte gauche est : " + this.interJoueur.getCarteGauche().getNom() + "\nVotre carte droite est : " + this.interJoueur.getCarteDroite().getNom()+"\n");
				System.out.println("Choisissez votre carte à échanger contre celle que vous venez de choisir\n1. " + this.interJoueur.getCarteGauche().getNom() +"\n2. " +this.interJoueur.getCarteDroite().getNom() +"\n");
				break;
			case CHOIX_EN_PLUS:
				System.out.println("Oh... On dirait qu'il vous reste encore "+this.interInt+" chances !\n");
				System.out.println("Voulez vous utiliser une de vos chances supplémentaires ?\n1. Oui\n2. Non");
				break;
			case OBLIGATION_RETOURNER_CARTE_GAUCHE:
				System.out.println("Vous retournez votre carte gauche: "+this.interCarteGauche.getNom()+" car votre carte droite est déjà retournée");
				break;
			case OBLIGATION_RETOURNER_CARTE_DROITE:
				System.out.println("Vous retournez votre carte droite: "+this.interCarteDroite.getNom()+" car votre carte gauche est déjà retournée");
				break;
			case CHOIX_CARTE_RETOURNER :
				System.out.println("Choisissez la carte a retourner \n1. " + this.interCarteGauche.getNom() +"\n2. " + this.interCarteDroite.getNom());
				break;
			case TRICK_A_REALISER :
				System.out.println("Le trick à réaliser est " + this.interString);
				break;
			case CHOIX_REAL_TRICK:
				System.out.println("Souhaitez-vous réaliser ce trick ? :\n1. Oui\n2. Non");
				break;
				
			}
		}
		
		else if(o instanceof ChoixExtension) {
			
			switch((Evenement)arg){
			
			case CHOIX_EXTENSION :
				System.out.println("Voulez-vous jouer avec le jeu de cartes par défaut, ou voulez-vous jouer avec une variante sur le thème de la gastronomie américaine ?\n1. Oui\n2. Non");
				break;
			case ERREUR_CHOIX_1_2 :
				System.out.println("Entrez une valeur correcte (Entre 1 et 2)");
				break;
			default :
				break;
			
			}
			
		}
		
	}
	
	public void setInterInt(int i) {
		
		this.interInt = i;
		
	}
	
	public void setInterString(String chaine) {
		
		this.interString = chaine;
		
	}
	
	public void setInterJoueur(Joueur j) {
		
		this.interJoueur = j;
		
	}
	
	public void setInterCarteGauche(Prop carteGauche) {
		
		this.interCarteGauche = carteGauche;
		
	}
	
public void setInterCarteDroite(Prop carteDroite) {
		
		this.interCarteDroite = carteDroite;
		
	}

}
