package projet_LO02;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Observable;
import java.util.Scanner;
import java.util.Set;

import Vues.*;

public class Joueur extends Observable{

	protected Date dateDeNaissance;
	protected String nom;
	protected String testString;
	protected Prop carteGauche;
	protected Prop carteDroite;
	protected int nbPoints;
	protected Strategy strat;
	protected VueConsole vc;
	protected VueGraphique vg;
	
	
	public Joueur(VueConsole v1, VueGraphique v2, Partie p) {
		
		this.vc = v1;
		this.vg = v2;
		this.addObserver(vc);
		this.addObserver(vg);
		
		vg.setInterJoueur(this);
		this.setChanged();
		this.notifyObservers(Evenement.SAISIE_NOM_jOUEUR);
		
		this.setTestString(p.getChoixVueConsole());
		this.nom = this.testString;
		
		while(this.nom.equals("")) {
			
			this.setChanged();
			this.notifyObservers(Evenement.ERREUR_SAISIE_NOM);
			this.setTestString(p.getChoixVueConsole());
			this.nom = this.testString;
			
		}
		
		boolean condition = false;
		
		do {
			this.setChanged();
			this.notifyObservers(Evenement.SAISIE_DATE_NAISSANCE);
			this.setTestString(p.getChoixVueConsole());
			String dateNaissance = this.testString;
			if(dateNaissance.matches("[0-9]{2}/[0-9]{2}/[0-9]{4}")){
				condition = true;
				SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
			    try {
					this.dateDeNaissance = f.parse(dateNaissance);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				this.setChanged();
				this.notifyObservers(Evenement.ERREUR_FORMAT_DATE);
			}
		}while(condition == false);
		
		this.nbPoints = 0;
		
		
	}
	
	public Joueur(String virtuel, VueConsole vc, VueGraphique vg) {
		
		this.vc = vc;
		this.vg = vg;
		this.addObserver(vc);
		this.addObserver(vg);
		
	}
	
	
	public void augmenterPoints(TrickPile pile, Partie p){
		
		this.nbPoints += pile.enleverCarte();
		
		this.setChanged();
		this.vc.setInterInt(this.getPoints());
		this.vg.setInterInt(this.getPoints());
		this.notifyObservers(Evenement.AFFICHAGE_POINTS);
		

	}
	
	public void diminuerPoints(int m){
		
		this.nbPoints -= m;
		

	}
	
	
	public int getPoints() {
		
		return this.nbPoints;
		
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getNom() {
		
		return this.nom;
		
	}
	
	public Prop getCarteGauche() {
		
		return this.carteGauche;
		
	}
	
	public void setCarteGauche(Prop p) {
		
		this.carteGauche = p;
		
	}
	
	public Prop getCarteDroite() {
		
		return this.carteDroite;
		
	}
	
	public void setCarteDroite(Prop p) {
		
		this.carteDroite = p;
		
	}
	
	public Strategy getStrat() {
		
		return this.strat;
		
	}
	
	public void saisieDateNaissanceGraphique(String dateN) {
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
	    try {
			this.dateDeNaissance = f.parse(dateN);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setTestString(String s) {
		this.testString = s;
	}
	
	public void setTestString(boolean vueConsole) {
		
		if(vueConsole) {
			
			Scanner sc = new Scanner(System.in);
			this.testString = sc.nextLine();
			
		}
		
	}
	
	
	
}