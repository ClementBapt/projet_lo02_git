package projet_LO02;

public interface Strategy {

	public void choisirTrick(TrickPile pile, TrickDeck deck, Partie p, Prop carteGauche, Prop carteDroite, Joueur j);
	public void preprarerProps(Joueur J1, Joueur J2, TrickPile pile, Partie p, Joueur j);
	public void tenterTrick(TrickDeck deck, TrickPile pile, Partie p, Prop carteGauche, Prop carteDroite, Joueur j);
	public void retournerUneCarte(Prop carteGauche, Prop carteDroite, Joueur j, Partie p);
	public void reussirTrick(TrickDeck deck, TrickPile pile, Partie p, Prop carteGauche, Prop carteDroite, Joueur j);
	
}
