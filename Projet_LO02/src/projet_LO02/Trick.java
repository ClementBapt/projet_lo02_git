package projet_LO02;

import java.util.Scanner;

import Vues.Evenement;

public class Trick extends Carte{
	
	protected String nom;
	protected int point;
	protected boolean aMalus;
	protected String[] besoinCarte1;
	protected String[] besoinCarte2;
	protected int malus1;
	protected int malus2;
	
	public Trick(String nom, int point, String besoinCarte11, String besoinCarte12, String besoinCarte21, String besoinCarte22, boolean aMalus, int malus1, int malus2 , String lien) {
		
		this.nom = nom;
		this.point = point;
		
		this.besoinCarte1 = new String[2];
		this.besoinCarte2 = new String[2];
		
		this.besoinCarte1[0] = besoinCarte11;
		this.besoinCarte1[1] = besoinCarte12;
		
		this.besoinCarte2[0] = besoinCarte21;
		this.besoinCarte2[1] = besoinCarte22;
		
		this.aMalus = aMalus;
		this.malus1 = malus1;
		this.malus2 = malus2;
		
		this.lienImage = lien;
		
		this.estVisible = false;
		
	}
	
	public String toString() {
		
		StringBuffer sb = new StringBuffer();
		sb.append(this.nom);
		sb.append(", les cartes necessaires a la realistaion de ce trick sont :\n\n");
		
		sb.append(this.besoinCarte1[0]);
		if(this.besoinCarte1[1].compareTo("null")!=0) {
			
			sb.append(" ou ");
			sb.append(this.besoinCarte1[1]);
			
		}
		
		sb.append("\n");
		sb.append("et\n");
		
		sb.append(this.besoinCarte2[0]);
		if(this.besoinCarte2[1].compareTo("null")!=0) {
			
			sb.append(" ou ");
			sb.append(this.besoinCarte2[1]);
			
		}
		
		return sb.toString();
		
		
	}
	
	public void setAMalus() {
		
	}
	
	public String getNom() {
		
		return this.nom;
		
	}
	
	public boolean estRealisable(Prop carteGauche, Prop carteDroite) {
		
		if((carteGauche.getNom().equalsIgnoreCase(this.besoinCarte1[0]))||(carteGauche.getNom().equalsIgnoreCase(this.besoinCarte1[1]))){
			
			if((carteDroite.getNom().equalsIgnoreCase(this.besoinCarte2[0]))||(carteDroite.getNom().equalsIgnoreCase(this.besoinCarte2[1]))) {
				
				
				this.setChanged();
				this.notifyObservers(Evenement.TRICK_REALISABLE);
				return true;
					
				}
				
			else {
				
				this.setChanged();
				this.notifyObservers(Evenement.TRICK_NON_REALISABLE);
				return false;
			}
			
	}
	
		else if((carteGauche.getNom().equalsIgnoreCase(this.besoinCarte2[0]))||(carteGauche.getNom().equalsIgnoreCase(this.besoinCarte2[1]))){
			 
			if((carteDroite.getNom().equalsIgnoreCase(this.besoinCarte1[0]))||(carteDroite.getNom().equalsIgnoreCase(this.besoinCarte1[1]))) {
				
				this.setChanged();
				this.notifyObservers(Evenement.TRICK_REALISABLE);
				return true;
				
		  }
			
			else {
				
				this.setChanged();
				this.notifyObservers(Evenement.TRICK_NON_REALISABLE);
				return false;
			}
			
		}
		
		else {
			
			this.setChanged();
			this.notifyObservers(Evenement.TRICK_NON_REALISABLE);
			return false;
			
		}
		
	}
	

}