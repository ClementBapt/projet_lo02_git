package projet_LO02;
import Vues.*;

public class JoueurReel extends Joueur{

	
	public JoueurReel(VueConsole vc, VueGraphique vg, Partie p) {
		
		super(vc, vg, p);
		
		this.strat = new StrategyReelle(vc, vg);
		
	}
	
	
	
}
