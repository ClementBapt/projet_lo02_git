package projet_LO02;

import java.util.Scanner;
import java.util.Observable;
import Vues.*;

public class StrategyReelle extends Observable implements Strategy{
	
	VueConsole vc;
	VueGraphique vg;
	
	public StrategyReelle(VueConsole vc, VueGraphique vg) {
		
		this.vc = vc;
		this.addObserver(vc);
		this.vg = vg;
		this.addObserver(vg);
		
	}
	
	
public void choisirTrick(TrickPile pile, TrickDeck deck, Partie p, Prop carteGauche, Prop carteDroite, Joueur j) {
		
		this.setChanged();
		this.vc.setInterCarteGauche(carteGauche);
		this.vc.setInterCarteDroite(carteDroite);
		this.notifyObservers(Evenement.DESCRIPTION_PROPS);
		this.setChanged();
		this.vc.setInterString(pile.getTopTrick().toString());
		this.notifyObservers(Evenement.TRICK_INITIAL);
		
		if(!deck.empty()) {
		
			this.setChanged();
			this.vg.setStratR(this);
			this.notifyObservers(Evenement.CHOIX_ACTION);
			
			p.setChoix(p.getChoixVueConsole());
			int choix = p.getChoix();
			
			
			while(choix < 1 || choix > 2) {
				
				this.setChanged();
				this.notifyObservers(Evenement.ERREUR_CHOIX_1_2);
				p.setChoix(p.getChoixVueConsole());
				choix = p.getChoix();
				
			}
			
			switch(choix) {
	
			case 2:
				
				pile.ajouterCarte(deck);
				
				if(deck.empty()) {
					
					p.incrementNbToursTOHT();
					
				}
				
				this.setChanged();
				this.vc.setInterString(pile.getTopTrick().toString());
				this.notifyObservers(Evenement.CARTE_DESSUS_PILE);
				break;
				}
		}
		else {
			
			this.setChanged();
			this.notifyObservers(Evenement.ERREUR_DECK_VIDE);
			p.incrementNbToursTOHT();
		}
		this.vg.actualiserTrick();

		
	}
	
	public void preprarerProps(Joueur J1, Joueur J2, TrickPile trick, Partie p, Joueur j) {
		
		this.setChanged();
		this.notifyObservers(Evenement.CARTES_SUR_LE_PLATEAU);
		
		this.setChanged();
		this.vc.setInterJoueur(J1);
		this.notifyObservers(Evenement.CARTES_DU_JOUEUR);
		if(J1.carteGauche.estVisible) {
			this.setChanged();
			this.vc.setInterJoueur(J1);
			this.notifyObservers(Evenement.DESCRIPTION_CARTE_GAUCHE);
		}
		else {
			this.setChanged();
			this.notifyObservers(Evenement.CARTE_GAUCHE_CACHEE);
		}
		if(J1.carteDroite.estVisible) {
			this.setChanged();
			this.vc.setInterJoueur(J1);
			this.notifyObservers(Evenement.DESCRIPTION_CARTE_DROITE);
		}
		else {
			this.setChanged();
			this.notifyObservers(Evenement.CARTE_DROITE_CACHEE);
		}
		
		this.setChanged();
		this.vc.setInterJoueur(J2);
		this.notifyObservers(Evenement.CARTES_DU_JOUEUR);
		if(J2.carteGauche.estVisible) {
			this.setChanged();
			this.vc.setInterJoueur(J2);
			this.notifyObservers(Evenement.DESCRIPTION_CARTE_GAUCHE_2);
		}
		else {
			this.setChanged();
			this.notifyObservers(Evenement.CARTE_GAUCHE_CACHEE_2);
		}
		if(J2.carteDroite.estVisible) {
			this.setChanged();
			this.vc.setInterJoueur(J2);
			this.notifyObservers(Evenement.DESCRIPTION_CARTE_DROITE_2);
		}
		else {
			this.setChanged();
			this.notifyObservers(Evenement.CARTE_DROITE_CACHEE_2);
		}
		
		this.setChanged();
		this.notifyObservers(Evenement.DEMANDE_CARTE);
		p.setChoix(p.getChoixVueConsole());
		int choix = p.getChoix();
		
		while(choix < 1 || choix > 4) {
			
			this.setChanged();
			this.notifyObservers(Evenement.ERREUR_CHOIX_1_4);
			p.setChoix(p.getChoixVueConsole());
			choix = p.getChoix();
			
		}
		
		this.setChanged();
		this.vc.setInterJoueur(j);
		this.notifyObservers(Evenement.ECHANGER_CARTE);
		p.setChoixEchange(p.getChoixVueConsole());
		int choixC = p.getChoixEchange();
		
		while(choixC < 1 || choixC > 2) {
			
			this.setChanged();
			this.notifyObservers(Evenement.ERREUR_CHOIX_1_2);
			p.setChoixEchange(p.getChoixVueConsole());
			choixC = p.getChoixEchange();
			
		}
		
		Prop inter;
		
		switch (choix) {
				
				case 1: if(choixC == 1) {
							inter = j.getCarteGauche();
							j.setCarteGauche(J1.getCarteGauche());
							J1.setCarteGauche(inter);
						}
						else if (choixC == 2) {
							inter = j.getCarteDroite();
							j.setCarteDroite(J1.getCarteGauche());
							J1.setCarteGauche(inter);
						}
						break;
				case 2: 
					if(choixC == 1) {
						inter = j.getCarteGauche();
						j.setCarteGauche(J1.getCarteDroite());
						J1.setCarteDroite(inter);
					}
					else if (choixC == 2) {
						inter = j.getCarteDroite();
						j.setCarteDroite(J1.getCarteDroite());
						J1.setCarteDroite(inter);
					}
					break;		
				case 3: if(choixC == 1) {
							inter = j.getCarteGauche();
							j.setCarteGauche(J2.getCarteGauche());
							J2.setCarteGauche(inter);
						}
						else if (choixC == 2) {
							inter = j.getCarteDroite();
							j.setCarteDroite(J2.getCarteGauche());
							J2.setCarteGauche(inter);
						}
						break;
				case 4: if(choixC == 1) {
							inter = j.getCarteGauche();
							j.setCarteGauche(J2.getCarteDroite());
							J2.setCarteDroite(inter);
						}
						else if (choixC == 2) {
							inter = j.getCarteDroite();
							j.setCarteDroite(J2.getCarteDroite());
							J2.setCarteDroite(inter);
						}
						break;
				}
		
		this.setChanged();
		this.notifyObservers(Evenement.ACTUALISER_PLATEAU);
		
		int choixEnPlus = p.getNbChancesEnPlus();
		
		while(choixEnPlus > 0) {
			
			this.setChanged();
			this.vc.setInterInt(choixEnPlus);
			this.vg.setInterInt(choixEnPlus);
			this.notifyObservers(Evenement.CHOIX_EN_PLUS);
			p.setChoix(p.getChoixVueConsole());
			choix = p.getChoix();
			
			while(choix < 1 || choix > 2) {
				
				this.setChanged();
				this.notifyObservers(Evenement.ERREUR_CHOIX_1_2);
				p.setChoix(p.getChoixVueConsole());
				choix = p.getChoix();
				
			}
			
			if(choix == 1) {
			
				this.setChanged();
				this.notifyObservers(Evenement.CARTES_SUR_LE_PLATEAU);
				
				this.setChanged();
				this.vc.setInterJoueur(J1);
				this.notifyObservers(Evenement.CARTES_DU_JOUEUR);
				if(J1.carteGauche.estVisible) {
					this.setChanged();
					this.vc.setInterJoueur(J1);
					this.notifyObservers(Evenement.DESCRIPTION_CARTE_GAUCHE);
				}
				else {
					this.setChanged();
					this.notifyObservers(Evenement.CARTE_GAUCHE_CACHEE);
				}
				if(J1.carteDroite.estVisible) {
					this.setChanged();
					this.vc.setInterJoueur(J1);
					this.notifyObservers(Evenement.DESCRIPTION_CARTE_DROITE);
				}
				else {
					this.setChanged();
					this.notifyObservers(Evenement.CARTE_DROITE_CACHEE);
				}
			
				this.setChanged();
				this.vc.setInterJoueur(J2);
				this.notifyObservers(Evenement.CARTES_DU_JOUEUR);
				if(J2.carteGauche.estVisible) {
					this.setChanged();
					this.vc.setInterJoueur(J2);
					this.notifyObservers(Evenement.DESCRIPTION_CARTE_GAUCHE_2);
				}
				else {
					this.setChanged();
					this.notifyObservers(Evenement.CARTE_GAUCHE_CACHEE_2);
				}
				if(J2.carteDroite.estVisible) {
					this.setChanged();
					this.vc.setInterJoueur(J2);
					this.notifyObservers(Evenement.DESCRIPTION_CARTE_DROITE_2);
				}
				else {
					this.setChanged();
					this.notifyObservers(Evenement.CARTE_DROITE_CACHEE_2);
				}
			
			this.setChanged();
			this.notifyObservers(Evenement.DEMANDE_CARTE);
			p.setChoix(p.getChoixVueConsole());
			choix = p.getChoix();
			
			while(choix < 1 || choix > 4) {
				
				this.setChanged();
				this.notifyObservers(Evenement.ERREUR_CHOIX_1_4);
				p.setChoix(p.getChoixVueConsole());
				choix = p.getChoix();
				
			}
			
			this.setChanged();
			this.vc.setInterJoueur(j);
			this.notifyObservers(Evenement.ECHANGER_CARTE);		
			p.setChoixEchange(p.getChoixVueConsole());
			choixC = p.getChoixEchange();
			
			while(choixC < 1 || choixC > 2) {
				
				this.setChanged();
				this.notifyObservers(Evenement.ERREUR_CHOIX_1_2);
				p.setChoixEchange(p.getChoixVueConsole());
				choixC = p.getChoixEchange();
			}
			
			
			switch (choix) {
			
			case 1: if(choixC == 1) {
						inter = j.getCarteGauche();
						j.setCarteGauche(J1.getCarteGauche());
						J1.setCarteGauche(inter);
					}
					else if (choixC == 2) {
						inter = j.getCarteDroite();
						j.setCarteDroite(J1.getCarteGauche());
						J1.setCarteGauche(inter);
					}
					break;
			case 2: 
				if(choixC == 1) {
					inter = j.getCarteGauche();
					j.setCarteGauche(J1.getCarteDroite());
					J1.setCarteDroite(inter);
				}
				else if (choixC == 2) {
					inter = j.getCarteDroite();
					j.setCarteDroite(J1.getCarteDroite());
					J1.setCarteDroite(inter);
				}
				break;		
			case 3: if(choixC == 1) {
						inter = j.getCarteGauche();
						j.setCarteGauche(J2.getCarteGauche());
						J2.setCarteGauche(inter);
					}
					else if (choixC == 2) {
						inter = j.getCarteDroite();
						j.setCarteDroite(J2.getCarteGauche());
						J2.setCarteGauche(inter);
					}
					break;
			case 4: if(choixC == 1) {
						inter = j.getCarteGauche();
						j.setCarteGauche(J2.getCarteDroite());
						J2.setCarteDroite(inter);
					}
					else if (choixC == 2) {
						inter = j.getCarteDroite();
						j.setCarteDroite(J2.getCarteDroite());
						J2.setCarteDroite(inter);
					}
					break;
			}
			
			choixEnPlus--;
			
			this.setChanged();
			this.notifyObservers(Evenement.ACTUALISER_PLATEAU);
			
			
			}
			else if(choix == 2) {
				
				choixEnPlus = 0;
				this.setChanged();
				this.notifyObservers(Evenement.ACTUALISER_PLATEAU);
				
			}
			
		}

	}
	
	public void tenterTrick(TrickDeck deck, TrickPile pile, Partie p, Prop carteGauche, Prop carteDroite, Joueur j) {
		
		this.setChanged();
		vc.setInterCarteDroite(carteDroite);
		vc.setInterCarteGauche(carteGauche);
		this.notifyObservers(Evenement.DESCRIPTION_PROPS);
		
		this.setChanged();
		vc.setInterString(pile.trickPile.peek().toString());
		this.notifyObservers(Evenement.TRICK_A_REALISER);
		
		if(pile.trickPile.peek().estRealisable(carteGauche, carteDroite)) {
			
			this.setChanged();
			this.notifyObservers(Evenement.CHOIX_REAL_TRICK);
			p.setChoix(p.getChoixVueConsole());
			int choix = p.getChoix();
			
			while(choix < 1 || choix > 2) {
				
				this.setChanged();
				this.notifyObservers(Evenement.ERREUR_CHOIX_1_2);
				p.setChoix(p.getChoixVueConsole());
				choix = p.getChoix();
				
			}
			
			switch(choix) {
			
			case 1: this.reussirTrick(deck, pile, p, carteGauche, carteDroite, j);
					break;
			default:
				break;
			
			}
			
			
		}
		
		else {
			
			this.retournerUneCarte(carteGauche, carteDroite, j, p);
			
		}
		
	}
	
	
	public void retournerUneCarte(Prop carteGauche, Prop carteDroite, Joueur j, Partie p) {
		
		this.setChanged();
		vc.setInterCarteDroite(carteDroite);
		vc.setInterCarteGauche(carteGauche);
		this.notifyObservers(Evenement.DESCRIPTION_PROPS);
		
		if(carteDroite.estVisible == false && carteGauche.estVisible == false) {
			
			this.setChanged();
			vc.setInterCarteDroite(carteDroite);
			vc.setInterCarteGauche(carteGauche);
			this.notifyObservers(Evenement.CHOIX_CARTE_RETOURNER);
			
			p.setChoixEchange(p.getChoixVueConsole());
			int choix = p.getChoixEchange();
			
			while(choix < 1 || choix > 2) {
				
				this.setChanged();
				this.notifyObservers(Evenement.ERREUR_CHOIX_1_2);
				p.setChoixEchange(p.getChoixVueConsole());
				choix = p.getChoixEchange();
				
			}
			
			switch (choix) {
			
			case 1: 
				carteGauche.estVisible = true;
				break;
					
			case 2: 
				carteDroite.estVisible = true;
				break;
			}
			
		}
		else if(carteGauche.estVisible && carteDroite.estVisible == false) {
			carteDroite.estVisible = true;
			this.setChanged();
			vc.setInterCarteDroite(carteDroite);
			this.notifyObservers(Evenement.OBLIGATION_RETOURNER_CARTE_DROITE);
		}
		else if(carteGauche.estVisible == false && carteDroite.estVisible) {
			carteGauche.estVisible = true;
			this.setChanged();
			vc.setInterCarteGauche(carteGauche);
			this.notifyObservers(Evenement.OBLIGATION_RETOURNER_CARTE_GAUCHE);
		}
		
	}
	
public void reussirTrick(TrickDeck deck, TrickPile pile, Partie p, Prop carteGauche, Prop carteDroite, Joueur j) {
		
		j.augmenterPoints(pile, p);
		if(pile.empty() == true) {
			
			pile.ajouterCarte(deck);
			
		}
		p.echangerSeventhProp(j);
		
		System.out.println(p.getNbToursTOHT());
		if(p.getNbToursTOHT()!=0) {
			
			p.arreterPartie();
			
		}
		
	}

}
