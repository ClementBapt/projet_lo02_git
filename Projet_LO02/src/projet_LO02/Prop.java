package projet_LO02;

/**
 * La classe Prop h�rite de la classe carte. Les cartes que les joueurs ont en mains pendant le jeu sont des 
 * instances de la classe Prop.
 * 
 * 
 * @author Coco
 *
 */

public class Prop extends Carte{
	
	/**
	 * L'attribut nom de la classe Prop correspond a l'apellation de la carte selon les r�gles du jeu.
	 * Le nom du prop peut changer selon l'implantation des r�gles par d�fauts ou d'une variante.
	 * La valeur de l'attribut nom permet la v�rificatio des conditions de r�alisation des tricks.
	 */
	
	private String nom;
	
	/**
	 * Le constructeur Prop assigne un nom pass en param�tre au Prop et assigne sa visibilit� à false.
	 * 
	 * @param nom
	 * Le param�tre pass� au constructeur provient du fichier contenant les noms de toutes les cartes selon 
	 * la variante impl�ment�e.
	 */
	
	public Prop(String nom, String lien){
		
		this.nom = nom;
		this.lienImage = lien;
		this.estVisible = false;
		
		
	}
	
	/**
	 * Retourne la valeur de l'attribut nom du Prop
	 */

	public String getNom() {
			
			return this.nom;
			
		}
}
