package projet_LO02;
import Vues.*;
import java.util.Date;
import java.util.Observable;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Partie extends Observable implements Runnable{
	
	private int nbJoueurs;
	private int nombreJoueursReels;
	private boolean estTerminee;
	private Prop seventhProp;
	private int nombreDeTours;
	private Joueur[] joueur;
	private int joueurActuel;
	private TrickDeck deck;
	private TrickPile pile;
	private int nbToursTOHT;
	private boolean aNbToursRestreint;
	private int nbToursMax;
	private int nbChanceEnPlus;
	private ChoixExtension choixExtension;
	private PartiePersonalisee personnalisation;
	private String cheminCSVTrick;
	private String cheminCSVProp;
	private VueConsole vc;
	private VueGraphique vg;
	private int choix;
	private int choixEchange; 
	private boolean choixVueConsole;
	
	public void construirePartie() throws IOException {
		
		this.vg.setPartie(this);
		
		this.setChanged();
		this.notifyObservers(Evenement.BIENVENUE);

		this.choixVueConsole = false;
		
		this.nbJoueurs = 3;
		this.joueurActuel = 0;
		this.nbToursTOHT=0;
		this.joueur = new Joueur[3];
		
		this.choixExtension = new ChoixExtension();
		this.choixExtension.addObserver(vc);
		this.choixExtension.addObserver(vg);
		this.choixExtension.choisirExtension(this);
		
		this.personnalisation = new PartiePersonalisee();
		this.personnalisation.addObserver(vc);
		this.personnalisation.addObserver(vg);
		this.configurerPartie(this);
		
		//Scanner sc = new Scanner(System.in);
		this.setChanged();
		this.notifyObservers(Evenement.CB_JOUEURS);
		
		this.setChoix(this.choixVueConsole);
		this.nombreJoueursReels = this.choix;
		
		while(this.nombreJoueursReels > 3 || this.nombreJoueursReels < 0) {
			
			this.setChanged();
			this.notifyObservers(Evenement.ERREUR_NB_JOUEURS);
			this.setChoix(this.choixVueConsole);
			this.nombreJoueursReels = this.choix;
			//this.nombreJoueursReels = sc.nextInt();
			
		}
		
		for(int i=0; i<this.nombreJoueursReels; i++) {
			
			this.setChanged();
			vc.setInterInt(i);
			vg.setInterInt(i);
			this.notifyObservers(Evenement.INDICE_SAISIE_JOUEUR);
			this.joueur[i] = new JoueurReel(this.vc, this.vg, this);
			
		}
		
		for(int i=this.nombreJoueursReels; i<this.nbJoueurs; i++) {
			
			this.joueur[i] = new JoueurVirtuel(this.vc, this.vg);
			
		}
		
		this.definirOrdreJoueur();
		
		this.deck = new TrickDeck(this.cheminCSVTrick, this.vc, this.vg);
		this.pile = new TrickPile(deck);
		
		this.distribuerLesProps(this.cheminCSVProp);
		
		this.nombreDeTours = 0;
		
		this.setChanged();
		this.vg.setInterJTab(joueur);
		this.notifyObservers(Evenement.DEBUT_PARTIE);

		//this.lancerPartie();
		
		
	}
	
	public Partie(VueConsole vc, VueGraphique vg) {
		
		this.vc = vc;
		this.vg = vg;
		this.addObserver(vc);
		this.addObserver(vg);
		
	}
	
	public void definirOrdreJoueur() {
		
		if (this.nombreJoueursReels >= 1) {
			
			int i = 0;
			Joueur inter;
			
			for(int k=0; k<3; k++) {
				
				i = 0;
			
			while(i < (this.nbJoueurs-1) && (this.joueur[i+1] instanceof JoueurVirtuel)==false) {
			
				if(this.joueur[i].dateDeNaissance.before(this.joueur[i+1].dateDeNaissance)) {
					
					inter = this.joueur[i+1];
					this.joueur[i+1] = this.joueur[i];
					this.joueur[i] = inter;
					
				}
				
				i++;
				
			}
			
		  }
			
		}
		
		
	}
	
	public void distribuerLesProps(String chemin) throws IOException {
		
			
		   BufferedReader fichier_source = new BufferedReader(new FileReader(chemin));
		   String chaine;
		   ArrayList<Prop> l = new ArrayList<Prop>();
		   Prop p;
		   

		 
		   while((chaine = fichier_source.readLine())!= null){
		      
		         String[] tabChaine = chaine.split(";");
		         p = new Prop(tabChaine[0], tabChaine[1]);
		         l.add(p);
		         
		         
		   }
		   
		   Collections.shuffle(l);
		   fichier_source.close();
		   
		   int compteurProp = 0;
		   
		   for(int i=0; i<3; i++) {
			   
			   joueur[i].carteDroite = l.get(compteurProp);
			   compteurProp++;
			   joueur[i].carteGauche = l.get(compteurProp);
			   compteurProp++;
			   
		   }
		   this.seventhProp = l.get(compteurProp);
		   this.seventhProp.estVisible = false;
			
	}
	
	public void lancerPartie() {
		
	while(this.estTerminee == false) {
		
	this.setChanged();
	this.vc.setInterString(this.joueur[this.joueurActuel].getNom());
	this.vg.setInterString(this.joueur[this.joueurActuel].getNom());
	this.notifyObservers(Evenement.DEBUT_TOUR);
	
	if(this.joueur[this.joueurActuel].getStrat() instanceof StrategyReelle && this.choixVueConsole) {
	
		this.setChanged();
		this.notifyObservers(Evenement.CHANGER_DE_VUE);
			
			this.setChoix(this.choixVueConsole);
			int choix = this.choix;
			
			while(choix < 1 || choix > 2) {
				
				this.setChanged();
				this.notifyObservers(Evenement.ERREUR_CHOIX_1_2);
				this.setChoix(this.choixVueConsole);
				choix = this.choix;
				
				
			}
			
			switch(choix) {
			
			case 2:
				
				this.changerDeVue(false);
				
				break;
				}
		}
	
			this.verifierEtatPartie();
			
			
			if(this.estTerminee == false) {
				
				this.lancerTour(this.joueur[this.joueurActuel]);
				
				this.passerJoueurSuivant();
				
					
			}
			
	}
			
				
				Joueur vainqueur = this.joueur[0];
				
				for(int i=1; i<this.nbJoueurs; i++) {
					
					if(vainqueur.nbPoints<this.joueur[i].nbPoints) {
						
						vainqueur = this.joueur[i];
						
					}
					
				}
				
				this.setChanged();
				this.vc.setInterJoueur(vainqueur);
				this.vg.setInterJoueur(vainqueur);
				this.notifyObservers(Evenement.VAINQUEUR);
				
	}
	
	public void verifierEtatPartie() {
		
		
		if(this.nombreDeTours == this.nbToursMax && this.aNbToursRestreint == true) {
			
			this.arreterPartie();
			
		}
		
		if(this.nbToursTOHT == 3) {
			
			this.setChanged();
			this.notifyObservers(Evenement.ANNONCE_3_TOURS_TOHT);
			
			if(this.pile.trickPile.peek().aMalus) {
				
				for(int i=0; i<this.nbJoueurs; i++) {
					
					if((joueur[i].getCarteGauche().getNom().compareTo(pile.trickPile.peek().besoinCarte1[0]))==0||(joueur[i].getCarteGauche().getNom().compareTo(pile.trickPile.peek().besoinCarte1[1])==0)||(joueur[i].getCarteGauche().getNom().compareTo(pile.trickPile.peek().besoinCarte2[0])==0||(joueur[i].getCarteGauche().getNom().compareTo(pile.trickPile.peek().besoinCarte2[1])==0))) {
						
						joueur[i].diminuerPoints(pile.trickPile.peek().malus1);
						this.setChanged();
						this.vc.setInterJoueur(joueur[i]);
						this.vc.setInterString(joueur[i].getCarteGauche().getNom());
						this.vc.setInterInt(pile.trickPile.peek().malus1);
						
						this.vg.setInterJoueur(joueur[i]);
						this.vg.setInterString(joueur[i].getCarteGauche().getNom());
						this.vg.setInterInt(pile.trickPile.peek().malus1);
						this.notifyObservers(Evenement.RETRANCHER_MALUS);
						
					}
					if((joueur[i].getCarteDroite().getNom().compareTo(pile.trickPile.peek().besoinCarte1[0]))==0||(joueur[i].getCarteDroite().getNom().compareTo(pile.trickPile.peek().besoinCarte1[1])==0)||(joueur[i].getCarteDroite().getNom().compareTo(pile.trickPile.peek().besoinCarte2[0])==0||(joueur[i].getCarteDroite().getNom().compareTo(pile.trickPile.peek().besoinCarte2[1])==0))) {
						
						joueur[i].diminuerPoints(pile.trickPile.peek().malus2);
						this.setChanged();
						this.vc.setInterJoueur(joueur[i]);
						this.vc.setInterString(joueur[i].getCarteDroite().getNom());
						this.vc.setInterInt(pile.trickPile.peek().malus2);
						
						this.vg.setInterJoueur(joueur[i]);
						this.vg.setInterString(joueur[i].getCarteDroite().getNom());
						this.vg.setInterInt(pile.trickPile.peek().malus2);
						this.notifyObservers(Evenement.RETRANCHER_MALUS);

					}
					
				}
				
			}
				
			this.arreterPartie();
		}
		
		
		
	}
	
	public void arreterPartie() {
		
		this.estTerminee = true;
	
	}
	
	public int getNbToursTOHT() {
		
		return this.nbToursTOHT;
		
	}
	
	public void incrementNbToursTOHT() {
		
		this.nbToursTOHT++;
		
	}
	
	public void changerInterface() {
	
	}
	
	
	public void lancerTour(Joueur j) {
		
		if(this.deck.nbCartesRestantes == 0) {
			
			this.nbToursTOHT++;
			
		}
		
		
		j.strat.choisirTrick(pile, deck, this, j.getCarteGauche(), j.getCarteDroite(), j);
		
		int joueurAdverse = 3 - this.joueurActuel;
		
		switch(joueurAdverse) {
		
		case 3:
			
			j.strat.preprarerProps(this.joueur[1], this.joueur[2], this.pile, this, j);
			
			break;
		
		case 2:
			
			j.strat.preprarerProps(this.joueur[0], this.joueur[2], this.pile, this, j);
			
			break;
			
		case 1:
			
			j.strat.preprarerProps(this.joueur[0], this.joueur[1], this.pile, this, j);
			
			break;
		
		}
		
		j.strat.tenterTrick(deck, pile, this, j.getCarteGauche(), j.getCarteDroite(), j);
		
		
	}
	
	public void passerJoueurSuivant() {
		
		this.joueurActuel++;
		
		if(this.joueurActuel == this.nbJoueurs) {
			
			this.joueurActuel = 0;
			this.nombreDeTours++;
			
		}
	
	}
	
	public void echangerSeventhProp(Joueur j) {
		
		this.setChanged();
		this.vc.setInterString(this.seventhProp.getNom());
		this.vc.setInterCarteGauche(j.getCarteGauche());
		this.vc.setInterCarteDroite(j.getCarteDroite());
		
		this.vg.setInterString(this.seventhProp.getNom());
		this.vg.setInterCarteGauche(j.getCarteGauche());
		this.vg.setInterCarteDroite(j.getCarteDroite());
		this.notifyObservers(Evenement.CHOIX_SEVENTH_PROP);


		this.setChoix(this.choixVueConsole);
		int choix = this.choixEchange
				;	
		while(choix < 1 || choix > 3) {
			
			this.setChanged();
			this.notifyObservers(Evenement.ERREUR_CHOIX_1_3);
			this.setChoix(this.choixVueConsole);
			choix = this.choixEchange;
			
		}
		
		Prop inter;
		
		switch (choix) {
		
		case 1: inter = this.seventhProp;
				this.seventhProp = j.getCarteGauche();
				j.setCarteGauche(inter);
				break;
				
		case 2: inter = this.seventhProp;
				this.seventhProp = j.getCarteDroite();
				j.setCarteDroite(inter);
				break;
		
		case 3: break;
		
		
		}
		
		
		j.getCarteGauche().cacher();
		j.getCarteDroite().cacher();
		this.seventhProp.cacher();
		
		this.setChanged();
		this.vc.setInterCarteDroite(j.getCarteDroite());
		this.vc.setInterCarteGauche(j.getCarteGauche());
		
		this.vg.setInterCarteDroite(j.getCarteDroite());
		this.vg.setInterCarteGauche(j.getCarteGauche());
		this.notifyObservers(Evenement.MELANGER_CARTES_SEVENTH_PROP);
        
		this.setChoix(this.choixVueConsole);
        choix = this.choix;
        
        while(choix < 1 || choix > 2) {
		
       	this.setChanged();
        this.notifyObservers(Evenement.ERREUR_CHOIX_1_2);
        this.setChoix(this.choixVueConsole);
		choix = this.choix;
			
		}
        
        switch(choix) {
        
        case 1: inter = j.getCarteGauche();
			    j.setCarteGauche(j.getCarteDroite());
			    j.setCarteDroite(inter);
			    break;
			    
        case 2: break;
        
        }
	
	}
	
	public void echangerSeventhProp(Joueur j, int choix) {

		if(choix == 0)
			choix++;
		
		Prop inter;
		
		switch (choix) {
		
		case 1: inter = this.seventhProp;
		this.seventhProp = j.getCarteGauche();
		j.setCarteGauche(inter);
		break;
		
		case 2: inter = this.seventhProp;
		this.seventhProp = j.getCarteDroite();
		j.setCarteDroite(inter);
		break;

		case 3: break;
		
		
		}
		
		
		j.getCarteGauche().cacher();
		j.getCarteDroite().cacher();
		this.seventhProp.cacher();
		
	}
	
	public void configurerPartie(Partie p) {
	
		//Scanner sc = new Scanner(System.in);
		
		this.setChanged();
		this.notifyObservers(Evenement.CHOIX_REGLES);
		this.setChoix(this.choixVueConsole);
		int choix = this.choix;
		
		while(choix < 1 || choix > 2) {
			
			this.setChanged();
			this.notifyObservers(Evenement.ERREUR_CHOIX_1_2);
			this.setChoix(this.choixVueConsole);
			choix = this.choix;
			
		}
		
		switch (choix) {
		
			default: 
				RegleDefaut.appliquerReglesDefaut(this);
				break;
				
			case 2:
				this.personnalisation.personnaliserPartie(this);
				break;
		
		}
		
	}
	
	public void setCheminCSVTrick(String chemin) {
		
		this.cheminCSVTrick = chemin;
		
	}
	
	public void setCheminCSVProp(String chemin) {
		
		this.cheminCSVProp = chemin;
		
	}
	
	public void setNbChancesEnPlus(int chance) {
		
		this.nbChanceEnPlus = chance;
		
	}
	
	public int getNbChancesEnPlus() {
		
		return this.nbChanceEnPlus;
		
	}
	
	public void setNbToursMax(int max) {
		
		this.nbToursMax = max;
		
	}
	
	public int getNbToursMax() {
		
		return this.nbToursMax;
		
	}
	
	public void setANbToursMax(boolean reponse) {
		
		this.aNbToursRestreint = reponse;
		
	}
	
	public boolean getANbToursMax() {
		
		return this.aNbToursRestreint;
		
	}
	
	public Prop getSeventhProp() {
		return this.seventhProp;
	}
	
	public boolean getDeckContent() {
		if(this.deck.empty())
			return true;
		else
			return false;
	}
	
	public Trick getFirstPileCard() {
		return this.pile.getTopTrick();
	}
	
	public int getJoueurActuel() {
		return this.joueurActuel;
	}
	
	/*public void choixNbJoueursGraphique(String nbJoueurs) {
		this.nombreJoueursReels = Integer.parseInt(nbJoueurs);
	}*/
	
	public void setChoixVueConsole(boolean choix) {
		
		this.choixVueConsole = choix;
		
	}
	
	public boolean getChoixVueConsole() {
		
		return this.choixVueConsole;
		
	}
	
	public void setChoix(boolean vueConsole) {
		
		if(vueConsole) {
			
			Scanner sc = new Scanner(System.in);
			this.choix = sc.nextInt();
			
		}
		
	}
	
	public void setChoix(int choix) {
		
		this.choix = choix;
		
	}
	
	public int getChoix() {
		
		return this.choix;
		
	}
	
	public void setChoixEchange(boolean vueConsole) {
		
		if(vueConsole) {
			
			Scanner sc = new Scanner(System.in);
			this.choixEchange = sc.nextInt();
			
		}
		
	}
	
	public void setChoixEchange(int choixEchange) {
		
		this.choixEchange = choixEchange;
		
	}
	
	public int getChoixEchange() {
		
		return this.choixEchange;
		
	}
	
	public void changerDeVue(boolean vueConsole) {
		
		this.choixVueConsole = vueConsole;
		
	}
	
	
	
	public void run() {
		
		try {
			this.construirePartie();}
		catch(IOException e) {
			
			e.printStackTrace();
			
		}
		this.lancerPartie();
		
	}
	
	
	
}