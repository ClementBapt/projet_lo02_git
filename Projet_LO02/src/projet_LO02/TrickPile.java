package projet_LO02;
import java.util.Stack;

/**
 * La classe TrickPile est une classe de stockage des diff�rents tricks de la partie.
 * @author Coco
 *
 */

public class TrickPile{


	/**
	 * L'attribut nombreCarteRestantes r�f�rence le nombre de carte qui ont �t�es retourn�es du trickdeck mais
	 * non r�alis�es.
	 */
	
	private int nombreCarteRestantes;
	
	/**
	 * L'attribut trickPile est une collection permettant de stocker les tricks de la partie.
	 * C'est cet attribut qui est manipul� tout au long de la partie pour acc�der � un trick de la pile.
	 */
	
	Stack<Trick> trickPile;
	
	/**
	 * Le constructeur TrickPile cr�e une collection et initialise le nombreCarteRestantes � 0. Pour finaliser 
	 * la mise en place du jeu, il fait appel � la m�thode ajouterCarte pour retourner le premier trick du jeu.
	 * 
	 * @param deck
	 * Le constructeur prends en param�tre le deck de la partie pour pouvoir le passer en param�tre lors de 
	 * l'appel � la m�thode ajouter carte.
	 */
	
	public TrickPile(TrickDeck deck) {
		
		this.trickPile = new Stack<Trick>();
		this.nombreCarteRestantes=0;
		this.ajouterCarte(deck);
		
		
		
	}
	
	/**
	 * La m�thode ajouter carte retourne le trick du dessus du trickDeck. Elle le supprime de ce dernier et 
	 * l'ajoute au-dessus de la pile.
	 * 
	 * @param deck
	 * Le constructeur prends en param�tre le deck de la partie pour pouvoir avoir acc�s � la carte du dessus 
	 * de celui-ci.
	 */
	
	public void ajouterCarte(TrickDeck deck) {
		
		if(deck.empty()==false) {
			this.trickPile.push(deck.trickQueue.removeFirst());
			this.trickPile.peek().estVisible = true;
			this.nombreCarteRestantes++;
		}
	}
	
	/**
	 * La m�thode enleverCarte permet de retirer le trick du dessus de la pile lors de la r�alisation de celui-ci.
	 * @return
	 * Retourne le nombre de points pour pouvoir l'ajouter aux points du joueur ayant effectu� le trick.
	 */
	
	public int enleverCarte() {
		
		this.nombreCarteRestantes--;
		return this.trickPile.pop().point;
	}
	
	/**
	 * 
	 * @return
	 * Retourne l'objet Trick du dessus de la pile
	 */
	
	public Trick getTopTrick() {
		
		return this.trickPile.peek();
		
	}
	
	/**
	 * La m�thode empty permet de v�rifier si la collection de trick est vide.
	 * 
	 * @return
	 * Elle retourne true si la collection est vide et false dans le cas contraire.
	 */
	
	public boolean empty() {
		if(this.trickPile.empty())
			return true;
		else
			return false;
	}

	
}

	