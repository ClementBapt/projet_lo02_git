package projet_LO02;

/**
 * La classe RegleDefaut permet de stocker et d'affecter directement les param�tres de jeu correspondants 
 * � la situation de jeu classique.
 * @author Coco
 *
 */

public class RegleDefaut{
	
	/**
	 * La m�thode appliquerReglesDefaut affecte � la partie actuelle les param�tres de jeu d'une utilisation 
	 * normale (sans variantes) du jeu.
	 * 
	 * @param p
	 * La m�thode prends la partie actuelle en param�tre et retourne void.
	 */
	
	public static void appliquerReglesDefaut(Partie p) {
		
		p.setANbToursMax(false);
		p.setNbChancesEnPlus(0);
		
		
	}

}
