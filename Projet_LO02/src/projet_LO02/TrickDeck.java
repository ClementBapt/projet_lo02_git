package projet_LO02;
import Vues.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

/**
 * La classe TrickDeck est une classe de stockage des diff�rents tricks de la partie.
 * 
 * 
 * @author Coco
 *
 */

public class TrickDeck {
	
	/**
	 * L'attribut trickQueue est une collection permettant de stocker les tricks de la partie.
	 * C'est cet attribut qui est manipul� tout au long de la partie pour retourner une carte du deck.
	 */
	
	protected LinkedList<Trick> trickQueue;
	
	/**
	 * L'attribut nbCartesRestantes est un compteur qui est d�cr�ment� au fur et � mesure qu'un trick est 
	 * retourn� du deck par un joueur.
	 * L'attribut est primordial dans la v�rification de la condition d'arr�t de la partie.
	 */
	
	protected int nbCartesRestantes;
	
	/**
	 * Le constructeur TrickDeck permet d'arranger les tricks dans une collection au lancement de la partie et de les m�langer pour avoir 
	 * un deck de jeu fonctionnel.
	 * 
	 * @param cheminAccesCSV
	 * La m�thode prends en param�tre une chaine correspondant au chemin vers le fichier contenant les 
	 * informations sur les cartes selon la variante choisie.
	 * @throws IOException
	 */
	
	public TrickDeck(String cheminAccesCSV, VueConsole vc, VueGraphique vg) throws IOException {
		
		   this.nbCartesRestantes = 0;
		
		   BufferedReader fichier_source = new BufferedReader(new FileReader(cheminAccesCSV));
		   String chaine;
		   this.trickQueue = new LinkedList<Trick>();
		   Trick t;
		   Trick toht;
		 
		   while((chaine = fichier_source.readLine())!= null){
		      
			     this.nbCartesRestantes++;
		         String[] tabChaine = chaine.split(";");
		         t = new Trick(tabChaine[0], Integer.parseInt(tabChaine[1]), tabChaine[2], tabChaine[3], tabChaine[4], tabChaine[5], Boolean.parseBoolean(tabChaine[6]), Integer.parseInt(tabChaine[7]), Integer.parseInt(tabChaine[8]), tabChaine[9]);
		         t.addObserver(vc);
		         t.addObserver(vg);
		         this.trickQueue.add(t);
		         
		   }
		   
		   toht = this.trickQueue.removeLast();
		   Collections.shuffle(this.trickQueue);
		   this.trickQueue.addLast(toht);
		   
		   fichier_source.close();
		   
		
	}
	
	/**
	 * La m�thode empty permet de v�rifier si la collection de trick est vide.
	 * Elle retourne true si la collection est vide et false dans le cas contraire.
	 * @return
	 */
	
	public boolean empty() {
		if(this.trickQueue.isEmpty())
			return true;
		else
			return false;
	}
	
}
