package projet_LO02;

import java.text.ParseException;
import Vues.*;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Iterator;

public class JoueurVirtuel extends Joueur{
	
	private static int numJoueurVirtuel = 0;
	
	public JoueurVirtuel(VueConsole v, VueGraphique v2) {
		
		super("", v, v2);
		
		JoueurVirtuel.numJoueurVirtuel++;
		
		StringBuffer sb = new StringBuffer();
		sb.append("Joueur Virtuel ");
		sb.append(JoueurVirtuel.numJoueurVirtuel);
		
		this.nom = sb.toString();
		
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
	    try {
			this.dateDeNaissance = f.parse("00/00/0000");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    this.nbPoints = 0;
	    this.strat = new StrategyVirtuelle(v, v2);
		
	}
}
