package projet_LO02;

import java.util.Observable;
import java.util.Scanner;

import Vues.Evenement;

/**
 * La classe ChoixExtension permet de choisir l'exension de carte avec laquelle les joueurs 
 * souhaitent jouer.
 * L'utlisation de cette class permet l'impl�mentation efficace d'un grand nombre potentiel de variantes de 
 * cartes.
 * 
 * Cette classe est exclusivement constitu�e d'attributs statiques corespondants aux chemins vers les 
 * fichiers contenants les informations sur les cartes.
 * 
 * Le format du fichier doit etre le .csv
 * 
 * En termes de s�curit�, le chemin pass� en tant qu'attribut de la classe doit �tre v�rifi� avec attention 
 * car une saisie erronn�e entrainera une erreur lors de la distribution des cartes.
 * 
 * 
 * @author Corentin
 *
 */

public class ChoixExtension extends Observable{
	
	/**
	 * Les attributs de la classe ChoisirExtension sont des objets statics String contenants les chemins 
	 * vers les fichiers de sp�cification des variantes de cartes.
	 * 
	 * Les valeurs des objets statiques sont des String uniquement et sont sp�cifi�es � l'ext�rieur de 
	 * l'appel du programme, il est donc du ressort du programmeur sp�cifiant le chemin d'�tre pr�cautionneux.
	 */
	
	private static String cheminCSVTrickDefaut = "tricks.csv";
	private static String cheminCSVPropDefaut = "props.csv";
	private static String cheminCSVTrickExt1 = "tricksBurger.csv";
	private static String cheminCSVPropExt1 = "propsBurger.csv";
	
	/**
	 * La m�thode choisirExtension est statique et retourne void, elle permet de choisir la variante de cartes � utiliser
	 *
	 * @param p
	 * Elle prends en param�tre la partie actuelle pour avoir acc�s aux attributs cheminCSVTrick et cheminCSVProp 
	 * de celle-ci.
	 */
	
	
	public void choisirExtension(Partie p) {
		
		this.setChanged();
		this.notifyObservers(Evenement.CHOIX_EXTENSION);
		
		//Scanner sc = new Scanner(System.in);
		
		p.setChoix(p.getChoixVueConsole());
		int choix = p.getChoix();
		
		
		while(choix < 1 || choix > 2) {
			
			this.setChanged();
			this.notifyObservers(Evenement.ERREUR_CHOIX_1_2);
			p.setChoix(p.getChoixVueConsole());
			choix = p.getChoix();
			
		}
		
		switch(choix) {
		
		case 1:
			p.setCheminCSVTrick(cheminCSVTrickDefaut);
			p.setCheminCSVProp(cheminCSVPropDefaut);
			break;
		case 2:
			p.setCheminCSVTrick(cheminCSVTrickExt1);
			p.setCheminCSVProp(cheminCSVPropExt1);
			break;
		
		}
		
	}
	
	public ChoixExtension() {
		
		
	}

}
