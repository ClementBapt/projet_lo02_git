package projet_LO02;

import java.util.Scanner;

import Vues.Evenement;

import java.util.Observable;

/**
 * La classe PartiePersonnalis�e est la classe qui permet de personnaliser sa partie au lancement de celle-ci, 
 * � savoir imposer un nombre de tours maximal ou octroyer une ou plusieurs chances suppl�mentaires aux joueurs 
 * lors de la pr�paration des props.
 * 
 * La partie actuelle est pass�e en param�tre et r�cup�re les options s�lectionn�es par le joueur.
 *   
 * @author Coco
 *
 */

public class PartiePersonalisee extends Observable {
	
	/**
	 * La m�thode personnaliserPartie permet de demander � l'utilisateur les options qu'il souhaite impl�menter 
	 * dans sa partie.
	 * 
	 * La m�thode retourne void. Elle n'est pas statique car on a besoin d'une instance Observable pour la classe d'affichage VueConsole.
	 * Les options possibles � modifier sont le nombre de tours maximal et le nombre de chances possible lors de la 
	 * pr�paration des props.
	 * 
	 * @param p
	 * La fonction prends en param�tre la partie actuelle et modifie les attributs correspondants dans la partie.
	 * 
	 */
	
	public void personnaliserPartie(Partie p) {
		
		this.setChanged();
		this.notifyObservers(Evenement.CONFIGURATEUR_PARTIE);
		
		p.setChoix(p.getChoixVueConsole());
		int choix = p.getChoix();
		
		while(choix < 1 || choix > 2) {
			
			this.setChanged();
			this.notifyObservers(Evenement.ERREUR_CHOIX_1_2);
			p.setChoix(p.getChoixVueConsole());
			choix = p.getChoix();
			
		}
		
		switch(choix) {
		
		case 1: p.setANbToursMax(true);
	
				this.setChanged();
				this.notifyObservers(Evenement.CHOIX_NB_TOURS_MAX);
				p.setChoix(p.getChoixVueConsole());
				choix = p.getChoix();
				
				while(choix < 1 || choix > 50) {
					
					this.setChanged();
					this.notifyObservers(Evenement.ERREUR_CHOIX_1_50);
					p.setChoix(p.getChoixVueConsole());
					choix = p.getChoix();
					
				}
				
				p.setNbToursMax(choix);
				break;
		case 2: p.setANbToursMax(false);
				break;
		}
		
		this.setChanged();
		this.notifyObservers(Evenement.CHANCES_SUP);
		p.setChoix(p.getChoixVueConsole());
		choix = p.getChoix();
		
		while(choix < 0 || choix >2) {
			
			this.setChanged();
			this.notifyObservers(Evenement.ERREUR_CHOIX_1_2);
			p.setChoix(p.getChoixVueConsole());
			choix = p.getChoix();
			
		}
		
		p.setNbChancesEnPlus(choix);
		
	}
	
	public PartiePersonalisee() {
		
		
		
	}

}
