package projet_LO02;

import java.util.Observable;

/**
 * La classe Carte est la classe dont h�ritent les classes Prop et Trick. C'est une classe de base et 
 * n'est jamais instanci�e.
 * 
 * Une carte a globalement deux �tats possibles, visible et non-visible. La visibilit� de la carte permet 
 * d'informer les joueurs r�els et virtuels de sa position dans le jeu.
 * @author Corentin DACAL et Cl�ment BAPTISTA
 *
 */

public class Carte extends Observable{
	
	/**
	 * L'attribut bool�en estVisible de la carte d�termine si la carte est face visible pour les joueurs 
	 * (estVisible = true) ou face cach�e (estVisible = false)
	 */
	

	protected boolean estVisible;
	protected String lienImage;
	
	public Carte(){
		
		
		
	}
	
	/**
	 * La m�thode montrer() est appel�e lorsqu'un joueur doit retourner une de ses cartes suite � l'�chec 
	 * de la r�alisation d'un trick.
	 * 
	 * La m�thode ne prends pas de param�tres et retoure void, elle change la valeur de l'attribut estVisible de la carte en question si celle-ci 
	 * est false, sinon elle ne fait rien.
	 */
	
	public void montrer() {
		
		if(this.estVisible == false) {
			
			this.estVisible = true;
			
		}
		
	}
	
	/**
	 * La m�thode cacher() est appel�e pour rendre non-visible une carte dont l'attribut estVisible est 
	 * true, suite � un trick r�ussi.
	 * 
	 * Le fonctionnement de la m�thode est similaire � celui de carte.montrer().
	 *
	 */
	
	public void cacher() {
		
		if(this.estVisible == true) {
			
			this.estVisible = false;
			
		}
		
	}
	
	public boolean getEstVisible() {
		if(this.estVisible)
			return true;
		else
			return false;
	}
	
	public String getLienImage() {
		
		return this.lienImage;
		
	}
	
	
}
