package projet_LO02;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import Vues.*;

public class StrategyVirtuelle extends Observable implements Strategy{
	
private VueConsole vc;
private VueGraphique vg;
	
public StrategyVirtuelle(VueConsole vc, VueGraphique vg) {
	
	this.vc = vc;
	this.addObserver(vc);
	
	this.vg = vg;
	this.addObserver(vg);
	
}
	
public void preprarerProps(Joueur J1, Joueur J2, TrickPile pile, Partie p, Joueur j) {
		
		Prop inter;
		Prop parcours;
		
		boolean changementProp = false;
		
		ArrayList<Prop> cartePlateau = new ArrayList<Prop>();
		cartePlateau.add(J1.carteGauche);
		cartePlateau.add(J1.carteDroite);
		cartePlateau.add(J2.carteGauche);
		cartePlateau.add(J2.carteDroite);
		
		int idCarteArray = 0;
		
		Iterator<Prop> itr = cartePlateau.iterator();
		
		
		if((j.getCarteGauche().getNom().compareTo(pile.trickPile.peek().besoinCarte1[0]))==0||(j.getCarteGauche().getNom().compareTo(pile.trickPile.peek().besoinCarte1[1])==0)){
			
			
			while((changementProp == false) && (itr.hasNext())) {
				
				parcours = itr.next();
				
				idCarteArray++;
				
				if((parcours.estVisible)&&(parcours.getNom().compareTo(pile.trickPile.peek().besoinCarte2[0])==0 || parcours.getNom().compareTo(pile.trickPile.peek().besoinCarte2[1])==0)) {
					
					changementProp = true;
					
					switch(idCarteArray) {
					
					case 1:
						inter = J1.getCarteGauche();
						J1.setCarteGauche(j.getCarteDroite());
						j.setCarteDroite(inter);
						break;
					case 2:
						inter = J1.getCarteDroite();
						J1.setCarteDroite(j.getCarteDroite());
						j.setCarteDroite(inter);
						break;
					case 3:
						inter = J2.getCarteGauche();
						J2.setCarteGauche(j.getCarteDroite());
						j.setCarteDroite(inter);
						break;
					case 4:
						inter = J2.getCarteDroite();
						J2.setCarteDroite(j.getCarteDroite());
						j.setCarteDroite(inter);
						break;
					
					
					}
					
					
				}
				
			}
			
			if(changementProp == false) {
				
				int choixCarteADonner = (int)(Math.random()*2);
				int choixCarteAPrendre = (int)(Math.random()*4);
				
				if(choixCarteADonner == 0) {
					
					switch(choixCarteAPrendre) {
					
					case 0:
						inter = J1.getCarteGauche();
						J1.setCarteGauche(j.getCarteGauche());
						j.setCarteGauche(inter);
						break;
					case 1:
						inter = J1.getCarteDroite();
						J1.setCarteDroite(j.getCarteGauche());
						j.setCarteGauche(inter);
						break;
					case 2:
						inter = J2.getCarteGauche();
						J2.setCarteGauche(j.getCarteGauche());
						j.setCarteGauche(inter);
						break;
					case 3:
						inter = J2.getCarteDroite();
						J2.setCarteDroite(j.getCarteGauche());
						j.setCarteGauche(inter);
						break;
					
					
					}
					
				}
				else if(choixCarteADonner == 1) {
					
					switch(choixCarteAPrendre) {
					
					case 0:
						inter = J1.getCarteGauche();
						J1.setCarteGauche(j.getCarteDroite());
						j.setCarteDroite(inter);
						break;
					case 1:
						inter = J1.getCarteDroite();
						J1.setCarteDroite(j.getCarteDroite());
						j.setCarteDroite(inter);
						break;
					case 2:
						inter = J2.getCarteGauche();
						J2.setCarteGauche(j.getCarteDroite());
						j.setCarteDroite(inter);
						break;
					case 3:
						inter = J2.getCarteDroite();
						J2.setCarteDroite(j.getCarteDroite());
						j.setCarteDroite(inter);
						break;
					
					
					}
					
				}
				
				
			}
			
			
		}
		
		
		else if((j.getCarteGauche().getNom().compareTo(pile.trickPile.peek().besoinCarte2[0]))==0||(j.getCarteGauche().getNom().compareTo(pile.trickPile.peek().besoinCarte2[1])==0)){
			
			while((changementProp == false) && (itr.hasNext())) {
				
				parcours = itr.next();
				
				idCarteArray++;
				
				if((parcours.estVisible)&&(parcours.getNom().compareTo(pile.trickPile.peek().besoinCarte1[0])==0 || parcours.getNom().compareTo(pile.trickPile.peek().besoinCarte1[1])==0)) {
					
					changementProp = true;
					
					switch(idCarteArray) {
					
					case 1:
						inter = J1.getCarteGauche();
						J1.setCarteGauche(j.getCarteDroite());
						j.setCarteDroite(inter);
						break;
					case 2:
						inter = J1.getCarteDroite();
						J1.setCarteDroite(j.getCarteDroite());
						j.setCarteDroite(inter);
						break;
					case 3:
						inter = J2.getCarteGauche();
						J2.setCarteGauche(j.getCarteDroite());
						j.setCarteDroite(inter);
						break;
					case 4:
						inter = J2.getCarteDroite();
						J2.setCarteDroite(j.getCarteDroite());
						j.setCarteDroite(inter);
						break;
					
					
					}
					
					
				}
				
			}
			
			if(changementProp == false) {
				
				int choixCarteADonner = (int)(Math.random()*2);
				int choixCarteAPrendre = (int)(Math.random()*4);
				
				if(choixCarteADonner == 0) {
					
					switch(choixCarteAPrendre) {
					
					case 0:
						inter = J1.getCarteGauche();
						J1.setCarteGauche(j.getCarteGauche());
						j.setCarteGauche(inter);
						break;
					case 1:
						inter = J1.getCarteDroite();
						J1.setCarteDroite(j.getCarteGauche());
						j.setCarteGauche(inter);
						break;
					case 2:
						inter = J2.getCarteGauche();
						J2.setCarteGauche(j.getCarteGauche());
						j.setCarteGauche(inter);
						break;
					case 3:
						inter = J2.getCarteDroite();
						J2.setCarteDroite(j.getCarteGauche());
						j.setCarteGauche(inter);
						break;
					
					
					}
					
				}
				else if(choixCarteADonner == 1) {
					
					switch(choixCarteAPrendre) {
					
					case 0:
						inter = J1.getCarteGauche();
						J1.setCarteGauche(j.getCarteDroite());
						j.setCarteDroite(inter);
						break;
					case 1:
						inter = J1.getCarteDroite();
						J1.setCarteDroite(j.getCarteDroite());
						j.setCarteDroite(inter);
						break;
					case 2:
						inter = J2.getCarteGauche();
						J2.setCarteGauche(j.getCarteDroite());
						j.setCarteDroite(inter);
						break;
					case 3:
						inter = J2.getCarteDroite();
						J2.setCarteDroite(j.getCarteDroite());
						j.setCarteDroite(inter);
						break;
					
					
					}
					
				}
				
				
			}
		 
			
		}
		
		else {
			
			int choixCarteADonner = (int)(Math.random()*2);
			int choixCarteAPrendre = (int)(Math.random()*4);
			
			if(choixCarteADonner == 0) {
				
				switch(choixCarteAPrendre) {
				
				case 0:
					inter = J1.getCarteGauche();
					J1.setCarteGauche(j.getCarteGauche());
					j.setCarteGauche(inter);
					break;
				case 1:
					inter = J1.getCarteDroite();
					J1.setCarteDroite(j.getCarteGauche());
					j.setCarteGauche(inter);
					break;
				case 2:
					inter = J2.getCarteGauche();
					J2.setCarteGauche(j.getCarteGauche());
					j.setCarteGauche(inter);
					break;
				case 3:
					inter = J2.getCarteDroite();
					J2.setCarteDroite(j.getCarteGauche());
					j.setCarteGauche(inter);
					break;
				
				
				}
				
			}
			else if(choixCarteADonner == 1) {
				
				switch(choixCarteAPrendre) {
				
				case 0:
					inter = J1.getCarteGauche();
					J1.setCarteGauche(j.getCarteDroite());
					j.setCarteDroite(inter);
					break;
				case 1:
					inter = J1.getCarteDroite();
					J1.setCarteDroite(j.getCarteDroite());
					j.setCarteDroite(inter);
					break;
				case 2:
					inter = J2.getCarteGauche();
					J2.setCarteGauche(j.getCarteDroite());
					j.setCarteDroite(inter);
					break;
				case 3:
					inter = J2.getCarteDroite();
					J2.setCarteDroite(j.getCarteDroite());
					j.setCarteDroite(inter);
					break;
				
				
				}
				
			}
			
		}
		
		this.setChanged();
		vc.setInterString(j.getNom());
		vg.setInterString(j.getNom());
		this.notifyObservers(Evenement.PREPARATION_PROPS_JVIRTUEL);
		
		try {Thread.sleep(1000);}
		catch(InterruptedException e) {e.printStackTrace();}
		
	}

	public void tenterTrick(TrickDeck deck, TrickPile pile, Partie p, Prop carteGauche, Prop carteDroite, Joueur j) {
		
		this.setChanged();
		vc.setInterString(pile.trickPile.peek().toString());
		this.notifyObservers(Evenement.TRICK_A_REALISER);
		
		if((carteGauche.getNom().compareTo(pile.trickPile.peek().besoinCarte1[0]))==0||(carteGauche.getNom().compareTo(pile.trickPile.peek().besoinCarte1[1])==0)){
			if((carteDroite.getNom().compareTo(pile.trickPile.peek().besoinCarte2[0]))==0||(carteDroite.getNom().compareTo(pile.trickPile.peek().besoinCarte2[1])==0)) {
				
				this.setChanged();
				vc.setInterString(j.getNom());
				vg.setInterString(j.getNom());
				this.notifyObservers(Evenement.PROPS_OK);
				
				if(Math.random() > 0.1)
					this.reussirTrick(deck, pile, p, carteGauche, carteDroite, j);

			}
			else {
				
				this.setChanged();
				vc.setInterString(j.getNom());
				vg.setInterString(j.getNom());
				this.notifyObservers(Evenement.PROPS_NOT_OK);
				this.retournerUneCarte(carteGauche, carteDroite, j, p);
			}
			
		}
		
		else if((carteGauche.getNom().compareTo(pile.trickPile.peek().besoinCarte2[0]))==0||(carteGauche.getNom().compareTo(pile.trickPile.peek().besoinCarte2[1])==0)){
			if((carteDroite.getNom().compareTo(pile.trickPile.peek().besoinCarte1[0]))==0||(carteDroite.getNom().compareTo(pile.trickPile.peek().besoinCarte1[1])==0)) {
				
				this.setChanged();
				vc.setInterString(j.getNom());
				vg.setInterString(j.getNom());
				this.notifyObservers(Evenement.PROPS_OK);
				
				if(Math.random() > 0.1)
					this.reussirTrick(deck, pile, p, carteGauche, carteDroite, j);
				
		  }
			
			else {
				
				this.setChanged();
				vc.setInterString(j.getNom());
				vg.setInterString(j.getNom());
				this.notifyObservers(Evenement.PROPS_NOT_OK);
				this.retournerUneCarte(carteGauche, carteDroite, j, p);
			}
			
		}
		
		else {
			this.setChanged();
			vc.setInterString(j.getNom());
			vg.setInterString(j.getNom());
			this.notifyObservers(Evenement.PROPS_NOT_OK);
			this.retournerUneCarte(carteGauche, carteDroite, j, p);
			
		}
		
		try {Thread.sleep(1000);}
		catch(InterruptedException e) {e.printStackTrace();}
		
	}
		
	public void choisirTrick(TrickPile pile, TrickDeck deck, Partie p, Prop carteGauche, Prop carteDroite, Joueur j) {
		
		if(deck.empty()==false) {
		
			if((carteGauche.getNom().equalsIgnoreCase(pile.trickPile.peek().besoinCarte1[0]))||(carteGauche.getNom().equalsIgnoreCase(pile.trickPile.peek().besoinCarte1[1]))){
				if((carteDroite.getNom().equalsIgnoreCase(pile.trickPile.peek().besoinCarte2[0]))||(carteDroite.getNom().equalsIgnoreCase(pile.trickPile.peek().besoinCarte2[1]))) {
					
					pile.ajouterCarte(deck);
					
					if(deck.empty()) {
						
						p.incrementNbToursTOHT();
						
					}
					
					this.setChanged();
					vc.setInterString(j.getNom());
					vg.setInterString(j.getNom());
					this.notifyObservers(Evenement.RETOURNER_CARTE_DECK);
					
				}
				else {				
					this.setChanged();
					vc.setInterString(j.getNom());
					vg.setInterString(j.getNom());
					this.notifyObservers(Evenement.RETOURNER_CARTE_PILE);
				}
				
			}
			
			else if((carteGauche.getNom().equalsIgnoreCase(pile.trickPile.peek().besoinCarte2[0]))||(carteGauche.getNom().equalsIgnoreCase(pile.trickPile.peek().besoinCarte2[1]))){
				if((carteDroite.getNom().equalsIgnoreCase(pile.trickPile.peek().besoinCarte1[0]))||(carteDroite.getNom().equalsIgnoreCase(pile.trickPile.peek().besoinCarte1[1]))) {
					
					pile.ajouterCarte(deck);
					
					if(deck.empty()) {
						
						p.incrementNbToursTOHT();
						
					}
					
					this.setChanged();
					vc.setInterString(j.getNom());
					vg.setInterString(j.getNom());
					this.notifyObservers(Evenement.RETOURNER_CARTE_DECK);
					
			  }
				
				else {
					this.setChanged();
					vc.setInterString(j.getNom());
					vg.setInterString(j.getNom());
					this.notifyObservers(Evenement.RETOURNER_CARTE_PILE);
				}
				
			}
			
			else {
				pile.ajouterCarte(deck);
				
				if(deck.empty()) {
					
					p.incrementNbToursTOHT();
					
				}
				
				this.setChanged();
				vc.setInterString(j.getNom());
				vg.setInterString(j.getNom());
				this.notifyObservers(Evenement.RETOURNER_CARTE_DECK);
			}
		  }
		else {
			
			this.setChanged();
			vc.setInterString(j.getNom());
			vg.setInterString(j.getNom());
			this.notifyObservers(Evenement.ERREUR_DECK_VIDE);
			p.incrementNbToursTOHT();
			System.out.println(p.getNbToursTOHT());
		}
		
		
		try {Thread.sleep(1000);}
		catch(InterruptedException e) {e.printStackTrace();}
		
	}
	
	public void retournerUneCarte(Prop carteGauche, Prop carteDroite, Joueur j, Partie p) {
		
		if(carteDroite.estVisible == false && carteGauche.estVisible == false) {
			if(Math.random() > 0.5) {
				carteDroite.montrer();
				this.setChanged();
				vc.setInterJoueur(j);
				vc.setInterCarteDroite(carteDroite);
				this.notifyObservers(Evenement.RETOURNER_CARTE_DROITE);
			}
			else {
				carteGauche.montrer();;
				this.setChanged();
				vc.setInterJoueur(j);
				vc.setInterCarteGauche(carteGauche);
				this.notifyObservers(Evenement.RETOURNER_CARTE_GAUCHE);
			}	
		}
		else if(carteGauche.estVisible && carteDroite.estVisible == false) {
			carteDroite.montrer();;
			this.setChanged();
			vc.setInterJoueur(j);
			vc.setInterCarteDroite(carteDroite);
			this.notifyObservers(Evenement.RETOURNER_CARTE_DROITE);
		}
		else if(carteGauche.estVisible == false && carteDroite.estVisible) {
			carteGauche.montrer();;
			this.setChanged();
			vc.setInterJoueur(j);
			vc.setInterCarteGauche(carteGauche);
			this.notifyObservers(Evenement.RETOURNER_CARTE_GAUCHE);
		}
		else if(carteDroite.estVisible && carteGauche.estVisible) {
			
			this.setChanged();
			this.notifyObservers(Evenement.CARTES_DEJA_RETOURNEES);
			
		}
		
		try {Thread.sleep(1000);}
		catch(InterruptedException e) {e.printStackTrace();}
		
	}
	
	public void reussirTrick(TrickDeck deck, TrickPile pile, Partie p, Prop carteGauche, Prop carteDroite, Joueur j) {
		
		j.augmenterPoints(pile, p);
		int choix = (int)Math.random()*4;
		p.echangerSeventhProp(j, choix);
		
		if(pile.empty() == true) {
			
			pile.ajouterCarte(deck);
			
		}
		
		this.setChanged();
		vc.setInterInt(j.getPoints());
		vc.setInterString(j.getNom());
		
		vg.setInterInt(j.getPoints());
		vg.setInterString(j.getNom());
		this.notifyObservers(Evenement.REUSSIR_TRICK);
		
		if(p.getNbToursTOHT()!=0) {
			
			p.arreterPartie();
			
		}
		
		try {Thread.sleep(1000);}
		catch(InterruptedException e) {e.printStackTrace();}
		
	}

}